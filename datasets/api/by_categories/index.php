<?php

    //
    // Retourne un JSON détaillant le contenu des 2 sous-dossiers ( pour chacun, la liste des images )
    // correspondant aux deux catégories choisies
    //

    // Ex : si les catégories choisies sont "Burger" et "Piano"
    // retourne le contenu de  : Burger/piano-burger et Piano/burger-piano


    $imagesFolder = $_GET['folder'];
    $dir_path = dirname(getcwd(),2) . '/' . $imagesFolder;
    $path = '../../' . $imagesFolder;

    if (! is_dir($dir_path)) {
        header('Content-Type: application/json');
        echo json_encode("Unkwown folder");
        die();
    }

    $category1 = $_GET['dataset1'];
    $category2 = $_GET['dataset2'];

    $folders = array();

    //
    // Catégorie 1
    //

    $path_category1 = $path . '/' . $category1;
    $category1_category2 = $category1 . '/' . strtolower($category2) . '-' . strtolower($category1);
    $path_category1_category2 = $path . '/' . $category1_category2;

    if ( file_exists($path_category1_category2) && is_dir($path_category1_category2)) {
        array_push($folders, $category1_category2);
    } else  if ( file_exists($path_category1) && is_dir($path_category1)) {
        array_push($folders, $category1);
    }

    //
    // Catégorie 2
    //

    $path_category2 = $path . '/' . $category2;
    $category2_category1 = $category2 . '/' . strtolower($category1) . '-' . strtolower($category2);
    $path_category2_category1 = $path . '/' . $category2_category1;

    if ( file_exists($path_category2_category1) && is_dir($path_category2_category1)) {
        array_push($folders, $category2_category1);
    } else  if ( file_exists($path_category2) && is_dir($path_category2)) {
        array_push($folders, $category2);
    }

    $result = (object) array();

    $datasets = array();

    foreach($folders as $folder) {

        $folderpath = $path . '/' . $folder;
        $folderParts = explode('/', $folder);

        $dataset = (object) array();
        $dataset->folder = reset($folderParts);
        $dataset->path = $imagesFolder . '/' . $folder;

        $files = array_values(array_filter(scandir($folderpath), function($file) use ($path) {
            return !is_dir($path . '/' . $file);
        }));

        $dataset_files = array();

        foreach($files as $file) {

            $dataset_file = (object) array();
            $dataset_file->name = $file;
            $dataset_file->path = $imagesFolder . '/' . $folder . '/' . $file;

            array_push($dataset_files, $dataset_file);
        }

        $dataset->files = $dataset_files;

        array_push($datasets, $dataset);
    }

    $result->datasets = $datasets;

    header('Content-Type: application/json');
    echo json_encode($result);

?>