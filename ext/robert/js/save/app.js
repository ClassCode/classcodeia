//LOAD ROBERT DATA
var robert;
$.ajax({ 
    type: 'GET', 
    url: 'data/robert.json',
    dataType: 'json',
    async: false,
    success: function (data) { 
        robert = data;
    }
});

//ADMIN
robert.forEach((item, index) => {
  $('#keywords').append('<li data-id="'+index+'"><h4>'+item.motsclefs.join(', ')+'</h4><!--div data-id="'+index+'">X</div--></li>');
});

var userBot = {
    "label":"user",
    "short":"U"
};

var robertBot = {
    "label":"robert",
    "short":"R2"
};

//Global variables
var talking=false;
var listening=false;
var timeoutListening;
var timeoutGeneral;

var tim;
var recognition = new webkitSpeechRecognition();
    recognition.lang = "fr-FR";
    recognition.continuous = true;
    recognition.interimResults = true;

recognition.onresult = function(event) {
    clearTimeout(tim);
    tim = setTimeout(function(){$("#realtime-speech").html("");},2500);
    var index = event.results.length-1;
    $("#realtime-speech").html(event.results[index][0].transcript);
    var match = event.results[index][0].transcript.toLowerCase(); //Sans attendre que ce soit final

    //event.results[index].isFinal    
    if(event.results[index].isFinal == true){
        $("#container").append(generateMsg(event.results[index][0].transcript,userBot,"Aucun mots clefs détectés"));
        smoothScroll();
    }

    //ANALYSE
    for(var i=0;i<robert.length;i++){
        for(var j=0;j<robert[i].motsclefs.length;j++){
            if(match.indexOf(robert[i].motsclefs[j].toLowerCase())!=-1){
                recognition.abort();
                
                console.log(robert[i].motsclefs[j].toLowerCase());

                var res = match.replace(robert[i].motsclefs[j].toLowerCase(), "<strong>"+robert[i].motsclefs[j].toLowerCase()+"</strong>");
                $("#container").append(generateMsg(res,userBot,null));

                var speech_text = robert[i].texte[getRandomInt(0,robert[i].texte.length)];
                speak(speech_text);
                $("#container").append(generateMsg(speech_text,robertBot,null));
                smoothScroll();
                /*$('#speech-bubble').show("slow", function(){
                    $(this).html(speech_text);
                });*/
            }
        }
    }
}
recognition.onend = function(){
    listening=false;
}
recognition.onstart = function(){
    listening=true;
}

function smoothScroll(){
    
    $("#chat").scrollTop($("#chat")[0].scrollHeight);

    /*var page = $(this).attr('href'); // Page cible
    var speed = 750; // Durée de l'animation (en ms)
    $('html, body').animate( { scrollTop: $(page).offset().top }, speed ); // Go
    return false;*/
}

//timeoutListening=setTimeout(relaunch,4000);
//timeoutGeneral=setTimeout(relaunchGene,180000);

function relaunch(){
    console.log("relaunch...");
    clearTimeout(timeoutListening);
    if(listening==false) {
        if(talking==false){
            recognition.start();
        }else{
            console.log("not relaunch because talking...");
        }
    }else{
        console.log("not relaunch because listening...");
    }
    timeoutListening=setTimeout(relaunch,4000);
}
function relaunchGene(){
    console.log("relaunchGene...");
    clearTimeout(timeoutGeneral);
    if(talking==false) {
        window.location.reload();
    }else{
        timeoutGeneral=setTimeout(relaunchGene,2000);
    }
}
function speak(text) {
    var voice = {
        "volume":1,
        "rate":1,
        "pitch":1,
        "type":"French Female"
    };
    responsiveVoice.speak(text, voice.type, {pitch:voice.pitch, rate:voice.rate, volume:voice.volume, onstart:StartCallback, onend:EndCallback});
}
function StartCallback(){
    talking=true;
    recognition.stop();
}
function EndCallback(){
    talking=false;
    recognition.start();
}

$('#go-robert').click(function(){
    //recognition.start();
    timeoutListening=setTimeout(relaunch,4000);
    timeoutGeneral=setTimeout(relaunchGene,180000);
    $(this).hide();
    
    var speechIntro = "Bonjour je suis Robert de Barettin !";
    speak(speechIntro);
    $("#container").append(generateMsg(speechIntro,robertBot,null));
});

function generateMsg(msg,user,comment){
    return  '<div class="msg-container '+user.label+'-bot">'+
            '   <div class="avatar"><span class="avater-label">'+user.short+'</span></div>'+
            '   <div class="msg">'+
            '       <div class="speech-bubble"></div>'+
            '       <p>'+msg+'</p>'+
            ( comment != null ? '       <p class="comment">'+comment+'</p>':'')+
            '   </div>'+
            '</div>';
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

/*–––––––––––— ADMIN –––––––––––—*/

var selectdId = null;

$("#admin button").click(function(){
    $('#keywords-editor').removeClass('hidden');
    $('#keywords-list').addClass('hidden');

    $('#keyword-input').append('<input type="text" name="motsclefs[]" required placeholder="…">');
    $('#reply').append('<textarea type="text" name="texte[]" required placeholder="…"></textarea>');
});

$("#keywords li").click(function(){
    $('#keywords-editor').removeClass('hidden');
    $('#keywords-list').addClass('hidden');

    //console.log(robert[$(this).data("id")]);

    selectdId = $(this).data("id");

    robert[selectdId].motsclefs.forEach((item, index) => {
        $('#keyword-input').append('<input type="text" name="motsclefs[]" value="'+item+'" required placeholder="…">');
    });

    robert[selectdId].texte.forEach((item, index) => {
        $('#reply').append('<textarea type="text" name="texte[]" required placeholder="…">'+item+'</textarea>');
    });
});

$("#btn_save").click(function(){

    $('#keywords-editor').addClass('hidden');
    $('#keywords-list').removeClass('hidden');

    //GET data
    var keywordData = {
        "motsclefs":[],
        "texte":[]
    };

    $('input[name="motsclefs[]"]').each(function() {
        if($(this).val() != ""){
            keywordData.motsclefs.push($(this).val());
        }
    });

    $('textarea[name="texte[]"]').each(function() { 
        if($(this).val() != ""){
            keywordData.texte.push($(this).val());
        }
    });

    robert[selectdId] = keywordData;

    //var motsclefs = $('input[name="motsclefs[]"]').val();
    //var texte = $('input[name="texte[]"]').val();
    
    console.log(keywordData);
    console.log(robert);

    //console.log(texte);
    $('#keywords li').remove();
    
    robert.forEach((item, index) => {
        $('#keywords').append('<li data-id="'+index+'"><h4>'+item.motsclefs.join(', ')+'</h4><!--div data-id="'+index+'">X</div--></li>');
    });

    //Process Error

    //&
    //Save here

    //&
    //Delate input
    $("#admin input").remove();
    $("#admin textarea").remove();
});

$('#admin').on( 'click change keyup keydown paste cut', 'textarea', function (){
    $(this).height(0).height(this.scrollHeight);
}).find( 'textarea' ).change();

//––––––––––––––––––––––––––––––
    
$('#add_keyword').click(function(){
    $('#keyword-input').append('<input type="text" name="motsclefs[]" value="" required placeholder="…">');
});

$('#add_reply').click(function(){
    $('#reply').append('<textarea type="text" name="texte[]" required placeholder="…"></textarea>');
});