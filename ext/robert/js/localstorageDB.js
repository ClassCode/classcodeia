// ========================================================================
//  STORE data
// ========================================================================
var storeJson="";


function storeData( theJson) {
    if (typeof(Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        localStorage.setItem("storeJson", theJson);
    } else {
        // Sorry! No Web Storage support..
    }
}

function restoreJson() {
    theData="";
    if (typeof(Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        theData=localStorage.getItem("storeJson");
        if(typeof theData != 'string') {
            theData="";
        }else{

        }
    } else {
        // Sorry! No Web Storage support..
        theData="notsupported";
    }
    return theData;
}

storeJson=restoreJson();

