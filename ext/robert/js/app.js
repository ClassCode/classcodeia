//LOAD ROBERT DATA
var robert;
$.ajax({
    cache: false,
    type: 'GET', 
    url: 'data/robert.json',
    dataType: 'json',
    async: false,
    success: function (data) {
        if(storeJson=="") {
            //robert = data;
            storeData(JSON.stringify(data) );
            storeJson=restoreJson();
        }else{
            //robert = JSON.parse(storeJson);
        }
        robert = JSON.parse(storeJson);
        //console.log(robert);
    }
});

//ADMIN
robert['data'].forEach((item, index) => {
  $('#keywords').append('<li data-id="'+index+'"><h4>'+item.keyword.join(', ')+'</h4><!--div data-id="'+index+'">X</div--></li>');
});

var userBot = {
    "label":"user",
    "short":"U"
};

var robertBot = {
    "label":"robert",
    "short":"R2"
};

//Global variables
var talking=false;
var listening=false;
var timeoutListening;

var delayFinal=1000;
var timeoutFinal;

var tim;
var recognition;
var timeoutResult;

function initRecognition(){

    try {
        recognition = new webkitSpeechRecognition();

        clearTimeout(timeoutResult);
        timeoutResult=setTimeout(messageHelp,15000);

        recognition.lang = "fr-FR";
        recognition.continuous = true;
        recognition.interimResults = true;

        recognition.onresult = function(event) {

            clearTimeout(timeoutResult);// ça a marché au moins une fois

            clearTimeout(tim);
            tim = setTimeout(function(){$("#realtime-speech").html("");},2500);
            var index = event.results.length-1;
            $("#realtime-speech").html(event.results[index][0].transcript);
            var match = event.results[index][0].transcript.toLowerCase(); //Sans attendre que ce soit final

            //event.results[index].isFinal
            if(event.results[index].isFinal == true){
                clearTimeout(timeoutFinal);
                timeoutFinal=setTimeout(confirmFinal, delayFinal,event.results[index][0].transcript)
            }

            //ANALYSE
            for(var i=0;i<robert['data'].length;i++){
                for(var j=0;j<robert['data'][i].keyword.length;j++){
                    if(match.indexOf(robert['data'][i].keyword[j].toLowerCase())!=-1){
                        recognition.abort();
                        clearTimeout(timeoutFinal);
                        //console.log(robert['data'][i].keyword[j].toLowerCase());

                        var res = match.replace(robert['data'][i].keyword[j].toLowerCase(), "<strong>"+robert['data'][i].keyword[j].toLowerCase()+"</strong>");
                        $("#container").append(generateMsg(res,userBot,null));

                        var speech_text = robert['data'][i].response[getRandomInt(0,robert['data'][i].response.length)];
                        speak(speech_text);
                        $("#container").append(generateMsg(speech_text,robertBot,null));
                        smoothScroll();
                        //-- communication avec le parent :
                        parent.postMessage("activeSuite", "*");
                    }
                }
            }
        }
        recognition.onend = function(){
            listening=false;
        }
        recognition.onstart = function(){
            listening=true;
        }
    } catch (error) {
        //à faire message que pas  possible du tout
        messageNotWorking();
    }

}

function messageNotWorking(){
    var speechIntro = "Votre navigateur ne supporte pas la synthèse et reconnaissance vocale utilisée.<BR />Vous pouvez essayer Chrome.";
    $("#container").append(generateMsg(speechIntro,robertBot,null));
}

function messageHelp(){
    var speechIntro = "Si vous parlez et que je n'entends pas, c'est que vous êtes peut-être sur firefox ou une version ancienne de Mac. Essayez sur Chrome peut-être ?";
    speak(speechIntro);
    $("#container").append(generateMsg(speechIntro,robertBot,null));
}


function confirmFinal(msg){
    $("#container").append(generateMsg(msg,userBot,"Aucun mot-clef détecté : va falloir ajouter ça à son vocabulaire !"));
    smoothScroll();
}


function smoothScroll(){
    $("#chat").scrollTop($("#chat")[0].scrollHeight);
}

function relaunch(){
    clearTimeout(timeoutListening);
    if(listening==false) {
        if(talking==false){
            try {
                recognition.stop();
            } catch (error) {
                console.error(error);
            }
            try {
                recognition.start();
            } catch (error) {
                console.error(error);
            }
        }
    }
    timeoutListening=setTimeout(relaunch,4000);
}

var synth = window.speechSynthesis;
var voice;

function speak(text){
    if (synth.speaking) {
        console.warn('speechSynthesis.speaking');
        return;
    } else {
        talking=true;
        recognition.stop();
    }

    if (text !== '') {    

        var utterance = new SpeechSynthesisUtterance(text);
            utterance.voice = voice;
            utterance.lang = robert.agent.lang;

        speechUtteranceChunker(utterance, {
            chunkLength: 120
        }, function () {
            console.log('SpeechSynthesisUtterance.onend');
            talking=false;
            recognition.stop();
            recognition.start();
        });
    }
}

$('#go-robert').click(function(){
    //recognition.start();
    initRecognition();

    var voices = synth.getVoices();
    voices.forEach((item, index) => { //Todo : Sinon prendre native
        if(item.name == robert.agent.voice){
            voice = item;
        }
    });

    timeoutListening=setTimeout(relaunch,4000);
    $(this).hide();
    
    var speechIntro = "Je vous écoute";
    speak(speechIntro);
    $("#container").append(generateMsg(speechIntro,robertBot,null));
    $('#console').css("display","block");
    parent.postMessage("microON", "*");
});

function generateMsg(msg,user,comment){
    return  '<div class="msg-container '+user.label+'-bot">'+
            '   <div class="avatar"><span class="avater-label">'+user.short+'</span></div>'+
            '   <div class="msg">'+
            '       <div class="speech-bubble"></div>'+
            '       <p>'+msg+'</p>'+
            ( comment != null ? '       <p class="comment">'+comment+'</p>':'')+
            '   </div>'+
            '</div>';
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

/*–––––––––––— ADMIN –––––––––––—*/

var selectdId = null;

$("#admin button").click(function(){
    $('#keywords-editor').removeClass('hidden');
    $('#keywords-list').addClass('hidden');

    $('#keyword-input').append('<input type="text" name="keyword[]" required placeholder="…">');
    $('#reply').append('<textarea type="text" name="response[]" required placeholder="…"></textarea>');
});

$("#keywords li").click(function(){

    alert("YO");

    $('#keywords-editor').removeClass('hidden');
    $('#keywords-list').addClass('hidden');

    //console.log(robert[$(this).data("id")]);

    selectdId = $(this).data("id");

    robert['data'][selectdId].keyword.forEach((item, index) => {
        $('#keyword-input').append('<input type="text" name="keyword[]" value="'+item+'" required placeholder="…">');
    });

    robert['data'][selectdId].response.forEach((item, index) => {
        $('#reply').append('<textarea type="text" name="response[]" required placeholder="…">'+item+'</textarea>');
    });
});

$("#btn_save").click(function(){

    $('#keywords-editor').addClass('hidden');
    $('#keywords-list').removeClass('hidden');

    //GET data
    var keywordData = {
        "keyword":[],
        "response":[]
    };

    $('input[name="keyword[]"]').each(function() {
        if($(this).val() != ""){
            keywordData.keyword.push($(this).val());
        }
    });

    $('textarea[name="response[]"]').each(function() { 
        if($(this).val() != ""){
            keywordData.response.push($(this).val());
        }
    });

    robert['data'][selectdId] = keywordData;

    //var keyword = $('input[name="keyword[]"]').val();
    //var response = $('input[name="response[]"]').val();
    
    //console.log(keywordData);
    //console.log(robert['data']);

    //console.log(teresponsexte);
    $('#keywords li').remove();
    
    robert['data'].forEach((item, index) => {
        $('#keywords').append('<li data-id="'+index+'"><h4>'+item.keyword.join(', ')+'</h4><!--div data-id="'+index+'">X</div--></li>');
    });

    //Process Error

    //&
    //Save here

    //&
    //Delate input
    $("#admin input").remove();
    $("#admin textarea").remove();
});

$('#admin').on( 'click change keyup keydown paste cut', 'textarea', function (){
    $(this).height(0).height(this.scrollHeight);
}).find( 'textarea' ).change();

//––––––––––––––––––––––––––––––
    
$('#add_keyword').click(function(){
    $('#keyword-input').append('<input type="text" name="keyword[]" value="" required placeholder="…">');
});

$('#add_reply').click(function(){
    $('#reply').append('<textarea type="text" name="response[]" required placeholder="…"></textarea>');
});

//-- admin or not ? ------------------
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}
function getUrlParam(parameter, defaultvalue){
    var urlparameter = defaultvalue;
    if(window.location.href.indexOf(parameter) > -1){
        urlparameter = getUrlVars()[parameter];
    }
    return urlparameter;
}

var causeOrAdmin = getUrlParam('mode','cause');
if(causeOrAdmin=='cause') {
    $('#bot').css('display','block');
    $('#admin').css('display','none');
}else{
    $('#bot').css('display','none');
    $('#admin').css('display','block');
}
window.onmessage = function (e) {

    if(e.data=="toggle"){
        document.location="./admin/index.php";
    }else{
    }

}

