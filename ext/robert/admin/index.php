<!--
	Sur mobile n'affiche pas robert de brétingy
	Sur mobie barre de recherche légérement buggé

	Ajouter un accès au mode d'emploi
	Ajouter un texte relatif à l'enregistrement des données et être conforme RGPD
	Ajouter une façon de visualiser la fonction d'apprentissage
		- Option confidentialité
		- Option hashage des données
-->
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>Admin - Robert de barretin</title>
	<meta id="myViewport" name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
	<script type="text/javascript" src="../js/lib/jquery.min.js"></script>
    <script type="text/javascript" src="../js/localstorageDB.js"></script>

    <link rel="stylesheet" href="admin.css">
</head>
<body>
	<header>
		<h1><a href="#">liste des mots ou expressions déclencheurs</a></h1>
		<a id="addButton" href="keyword.php?action=add"><div class="addIcon"></div></a>
		<input type="text" class="hidden" id="dynamicSearchInput" placeholder="Rechercher…">
		<button id="searchButton"><div class="searchIcon"></div></button>
	</header>
	<section>
		<ul id="list"></ul>
	</section>
	<footer>
		<p>© data.bingo</p>
	</footer>
	<script>
        //-- saveJson en localStorage ? ------------------
        function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        }
        function getUrlParam(parameter, defaultvalue){
            var urlparameter = defaultvalue;
            if(window.location.href.indexOf(parameter) > -1){
                urlparameter = getUrlVars()[parameter];
            }
            return urlparameter;
        }
        //var saveJson= getUrlParam('saveJson','');
        //console.log("saveJson");
        //console.log(saveJson);
		//LOAD ROBERT DATA
		var robert;
		$.ajax({
			cache: false,
			type: 'GET', 
			url: '../data/robert.json',
			dataType: 'json',
			async: false,
			success: function (data) {
                if(storeJson=="") {
                    //robert = data;
                    storeData(JSON.stringify(data) );
                    storeJson=restoreJson();
                }else{
                    //robert = JSON.parse(storeJson);
                }
                robert = JSON.parse(storeJson);
			}
		});
		
		//Alphabetic list // En cours pour créer une liste alphabétique comme sur Iphone plus facile pour naviger
		/*robert.data.sort(function(a, b){
		    if(a.keyword[0] < b.keyword[0]) { return -1; }
		    if(a.keyword[0] > b.keyword[0]) { return 1; }
		    return 0;
		});*/

		//robert.data.sort((a, b) => a.keyword[0].localeCompare(b.keyword[0]));

		$(document).ready(function(){
		});

		robert.data.forEach((item, index) => {
			$('#list').append('<li><a href="keyword.php?action=edit&id='+index+'">'+item.keyword.join(', ')+'</a></li>');
		});

		var searchIsActive = false;

		$('#searchButton').click(function(){
			$('#searchButton div').toggleClass("searchIcon closeIcon");
			$('#dynamicSearchInput').toggleClass("hidden");
			$('#dynamicSearchInput').focus();
			searchIsActive = !searchIsActive;

			if(searchIsActive == false){
				$("#dynamicSearchInput").val("");
				$('#list').empty();
				robert.data.forEach((item, index) => {
					$('#list').append('<li><a href="keyword.php?action=edit&id='+index+'"><h4>'+item.keyword.join(', ')+'</h4></a></li>');
				});
			}
		});

		//Pas opti mais fonctionne bien
		$('#dynamicSearchInput').on('change keyup keydown paste cut', function (){

			var query = $.trim(this.value.toLowerCase());

			if(query != '' && query != ' '){
				var result = [];
				var k = 0;
				for(var i=0;i<robert.data.length;i++){
					for(var j=0;j<robert.data[i].keyword.length;j++){
						if(robert.data[i].keyword[j].toLowerCase().indexOf(query)==0){
							result[k] = robert.data[i];
							result[k]['id'] = i;
							k++;
							break;
						}
					}
				}

				$('#list').empty();
				result.forEach((item, index) => {
					$('#list').append('<li><a href="keyword.php?action=edit&id='+result[index]['id']+'">'+item.keyword.join(', ')+'</a></li>');
				});

				if(result.length === 0){
					$('#list').append('<li><a href="keyword.php?action=add&query='+query+'">Créer le mot déclencheur : '+query+'</a></li>');
				}
			} else {

				$('#list').empty();
				robert.data.forEach((item, index) => {
					$('#list').append('<li><a href="keyword.php?action=edit&id='+index+'">'+item.keyword.join(', ')+'</a></li>');
				});
			}
		});

        window.onmessage = function (e) {

            if(e.data=="toggle"){
                document.location="../index.html";
            }else{
            }

        }
	</script>
</body>
</html>