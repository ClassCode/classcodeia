let video;
let objectDetector; //v2
let yolo;//v1
let status;
let objects = [];

let vWidth = 0;//v1
let vHeight = 0;//v1
let modelLoaded = 0;//v1
let interpretations = [];//v1
let messageOn=0;//v1

let canvas, ctx;//v2
const width = 640;//v2
const height = 480;//v2

async function make() {
    // get the video
    video = await getVideo();

    objectDetector = await ml5.objectDetector('cocossd', startDetecting)

    canvas = createCanvas(width, height);
    ctx = canvas.getContext('2d');

    // Hide the original video
    //video.hide();
    status = document.getElementById('status');
}

// when the dom is loaded, call make();
window.addEventListener('DOMContentLoaded', function() {
    make();
});


function draw() {
    if ((video.width == 0) || (modelLoaded == 0)) {
        /*background("#FFFFFF");
        fill(0, 0, 0);
        textSize(24);
        textAlign(CENTER);
        text("Chargement en cours...", 0, 230, 640, 200);*/

    } else {
        if (vWidth == 0) {
            let prop = video.height / video.width;
            vWidth = 640;
            vHeight = Math.floor(640 * prop);
        } else {
            // already set
        }
        ctx.fillStyle = "#000000"
        ctx.fillRect(0,0, width, height);

        ctx.drawImage(video, 0, 0);
        if (typeof objects === 'undefined') {
            // pb
        }else{
            for (let i = 0; i < objects.length; i++) {
                let textWidth = 30;

                if (typeof robert['data'] === 'undefined') {
                    // robert pas encore chargé
                }else{
                    var objects_match = objects[i].label.toLowerCase();
                    interpretations[i]=objects[i].label;
                    for (var j = 0; j < robert['data'].length; j++) {
                        if (objects_match.indexOf(robert['data'][j].keyword[0].toLowerCase()) != -1) {
                            if(robert['data'][j].response[0]=="") {
                                // on laisse interpretations[k]=objects[k].label
                            }else {
                                interpretations[i]=robert['data'][j].response[0];
                            }
                        }else{
                            // pas la bonne interpretation
                        }
                    }
                }
                if (typeof interpretations[i] === 'undefined') {
                    interpretations[i]="...";
                }else{
                }
                textWidth = interpretations[i].length;

                ctx.fillStyle = "#00FF00";
                //ctx.fillRect(objects[i].x * vWidth, objects[i].y * vHeight, textWidth * 12, 30);
                ctx.fillRect(objects[i].x, objects[i].y,textWidth * 12, 30);

                ctx.font = "16px Arial";
                ctx.fillStyle = "#000000";
                ctx.fillText(interpretations[i], objects[i].x + 4, objects[i].y + 16);

                ctx.beginPath();
                ctx.lineWidth = 4;
                ctx.rect(objects[i].x, objects[i].y, objects[i].width, objects[i].height);
                ctx.strokeStyle = "#00FF00";
                ctx.stroke();
                ctx.closePath();
            }
            //-- communication avec le parent :
            if(messageOn==0) {
                messageOn=1;
                parent.postMessage("activeSuite", "*");
            }else{
            }

        }

    }


}

function startDetecting() {
    status.innerHTML='Model loaded!';
    status.remove();

    modelLoaded = 1;
    detect();
}

function detect() {

    objectDetector.detect(video, function(err, results) {

        if(err){
            objects=[];
        }else{
            objects = results;
        }

        draw();
        detect();
    });
}

//LOAD YOLO DATA
var robert;

$.ajax({
    cache: false,
    type: 'GET',
    url: 'data/fakeyolo.json',
    dataType: 'json',
    async: false,
    success: function (data) {
        if (storeYoloJson == "") {
            //robert = data;
            storeData(JSON.stringify(data));
            storeYoloJson = restoreJson();
        } else {
            //robert = JSON.parse(storeJson);
        }
        robert = JSON.parse(storeYoloJson);
        console.log(robert);
    }
});


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}


window.onmessage = function (e) {

    if(e.data=="toggle"){
        document.location="./admin/index.php";
    }else{
    }

}




// Helper Functions
async function getVideo(){
    // Grab elements, create settings, etc.
    const videoElement = document.createElement('video');
    videoElement.setAttribute("style", "display: none;");
    videoElement.width = width;
    videoElement.height = height;
    document.body.appendChild(videoElement);

    // Create a webcam capture
    const capture = await navigator.mediaDevices.getUserMedia({ video: true })
    videoElement.srcObject = capture;
    videoElement.play();

    //console.log(videoElement.width)

    return videoElement

}

function createCanvas(w, h){
    const canvas = document.createElement("canvas");
    canvas.width  = w;
    canvas.height = h;
    document.body.appendChild(canvas);
    return canvas;
}


