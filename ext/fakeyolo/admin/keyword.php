<?php
/*
	if ($_SERVER['REQUEST_METHOD'] === 'POST'){

		$data = $_POST;
		unset($data['enregistrer']);
		unset($data['supprimer']);

		//Reload robert
		$updatedRobert = json_decode(file_get_contents("../data/robert.json"), true);
		
		if (isset($_POST['enregistrer'])){
			//traitement des données
			if($action == "edit"){

				//todo récupérer les valeurs en cours
				//$updatedRobert['data'][$id]['keyword'] = array_merge_recursive($updatedRobert['data'][$id]['keyword'], $data);
				//$updatedRobert['data'][$id]['response'] = array_merge_recursive($updatedRobert['data'][$id]['response'], $data);
				
				//Good old way
				$updatedRobert['data'][$id] = $data;
				
			} else if($action == "add"){
				array_push($updatedRobert['data'], $data);
			}
		} else if (isset($_POST['supprimer'])) {
			//Supprimer l'entrée du tableau
			array_splice($updatedRobert['data'], $id,1);
		}

		//header ('Location: index.php');
var_dump($_POST);
		//echo json_encode($data);
		exit;
	}
*/
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>Admin - Fakeyolo</title>
	<meta id="myViewport" name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
    <script type="text/javascript" src="../js/lib/jquery.min.js"></script>
    <script type="text/javascript" src="../js/localstorageDB.js"></script>
	<link rel="stylesheet" href="admin.css">
</head>
<body>
	<header>
		<h1><a href="index.php">Modifier les labels</a></h1>
	</header>

	<section>
		<form id="project" >
			<div class="entry">
				<div id="keywords">
				<label>Le label de yolo :</label>
					<input type="text" name="keyword[]" required placeholder="…" disabled>
					<p id="keywordError"></p>
				</div>
			</div>
	
			<div class="entry">
				<div id="reply">
					<label>Votre label :</label>
					<textarea type="text" name="response[]" required placeholder="…"></textarea>
				</div>
                <!--
				<button class="btn" id="add_reply">Ajouter une interprétation</button>
				-->
			</div>
        </form>
			<div class="entry" id="submit">
                <button id="btn_cancel" onclick="javascript:document.location='index.php';">Annuler</button>
                <button id="btn_save" >Enregistrer</button>
			</div>


	</section>

	<script type="text/javascript">

		//LOAD ROBERT DATA
		var robert;
		$.ajax({
			cache: false,
			type: 'GET', 
			url: '../data/fakeyolo.json',
			dataType: 'json',
			async: false,
			success: function (data) {
                if(storeYoloJson=="") {
                    //robert = data;
                    storeData(JSON.stringify(data) );
                    storeYoloJson=restoreJson();
                }else{
                    //robert = JSON.parse(storeJson);
                }
                robert = JSON.parse(storeYoloJson);
			}
		});
		
		$(document).ready(function(){
			//$('header h1 a').html(robert.agent.name);
			document.title = 'Admin - '+robert.agent.name;

			var action = (_GET('action')||'add')=='edit'?'edit':'add';
			var id = action == 'edit' ? typeof _GET('id') == 'string' ? ( _GET('id')<robert.data.length ? _GET('id') : robert.data.length-1 ) : 0 : robert.data.length;
			var keyword = action == "edit" ? robert.data[id] : null;
			var query = typeof _GET('query') == 'string' ? _GET('query') : null;

			if(action == 'edit'){
				
				$('#keywords input').val(keyword.keyword[0]);

				if(keyword.keyword.length > 1){
					for (var i = 1; i < keyword.keyword.length; i++) {
						$('#keywords').append('<input type="text" name="keyword[]" value="'+keyword.keyword[i]+'" required placeholder="…">');
					}
				}

				$('#reply textarea').val(keyword.response[0]);
				//$(this).height(0).height(this.scrollHeight); //TODO
				
				if(keyword.response.length > 1){
					//console.log('hello');
					for (var i = 1; i < keyword.response.length; i++) {
						$('#reply').append('<textarea type="text" name="response[]" required placeholder="…">'+keyword.response[i]+'</textarea>');
					}
				}

				/*$('#submit').append('<button id="btn_delete" >Suprimer</button>');

                $('#btn_delete').click(function(){
                    //console.log(robert.data);

                    let myObject={"response":[],"keyword":[]}
                    $('input[name^="keyword"]').each(function() {
                        myObject.keyword[myObject.keyword.length]=$(this).val().normalize();
                    });
                    myObject.response[myObject.response.length]="";

                    robert.data[currentId]=myObject;

                    storeData(JSON.stringify(robert) );
                    document.location='index.php';
                    //console.log(robert.data);
                });*/
			
			} else if(action == 'add'){
				if(query != null){
					$('#keywords input').val(query);
				}				
			}


		});

		$.fn.serializeControls = function() {
		  var data = {};
		
		  function buildInputObject(arr, val) {
		    if (arr.length < 1)
		      return val;  
		    var objkey = arr[0];
		    if (objkey.slice(-1) == "]") {
		      objkey = objkey.slice(0,-1);
		    }  
		    var result = {};
		    if (arr.length == 1){
		      result[objkey] = val;
		    } else {
		      arr.shift();
		      var nestedVal = buildInputObject(arr,val);
		      result[objkey] = nestedVal;
		    }
		    return result;
		  }
		
		  $.each(this.serializeArray(), function() {
		    var val = this.value;
		    var c = this.name.split("[");
		    var a = buildInputObject(c, val);
		    $.extend(true, data, a);
		  });
		  
		  return data;
		}



		$('section').on( 'change keyup keydown paste cut', 'textarea', function (){
			$(this).height(0).height(this.scrollHeight);
		}).find( 'textarea' ).change();

		//––––––––––––––––––––––––––––––
			
		$('#add_keyword').click(function(){
			$('#keywords').append('<input type="text" name="keyword[]" value="" required placeholder="…">');
		});

        $('#add_reply').click(function(){
            $('#reply').append('<textarea type="text" name="response[]" required placeholder="…"></textarea>');
        });
        $('#btn_save').click(function(){
            let myObject={"response":[],"keyword":[]}
            $('input[name^="keyword"]').each(function() {
                myObject.keyword[myObject.keyword.length]=$(this).val().normalize();
            });
            $('textarea[name^="response"]').each(function() {
                myObject.response[myObject.response.length]=$(this).val().normalize();
            });


            if(currentId==-1) {//!!!!!!!!!!!! à corriger dans Robert
                robert.data.splice(0, 0, myObject);
            }else{
                robert.data[currentId]=myObject;
            }

            storeData(JSON.stringify(robert) );
            document.location='index.php';
            //console.log(robert.data);
        });


		//––––––––––––––––––––––––––––––

		function validateForm(){
			
			/*e.preventDefault();

			var formValues = document.forms["project"]["fname"].value;
			
			console.log(formValues);

			if (formValues == "") {
				alert("Veuillez remplir correctement tous les champs");
				return false;
			}*/
		}

		function _GET(name, url) {
			if (!url) url = window.location.href;
			name = name.replace(/[\[\]]/g, "\\$&");
			var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
				results = regex.exec(url);
			if (!results) return null;
			if (!results[2]) return '';
			return decodeURIComponent(results[2].replace(/\+/g, " "));
		}


        //-- admin or not ? ------------------
        function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        }
        function getUrlParam(parameter, defaultvalue){
            var urlparameter = defaultvalue;
            if(window.location.href.indexOf(parameter) > -1){
                urlparameter = getUrlVars()[parameter];
            }
            return urlparameter;
        }
        var currentId= getUrlParam('id','0');
        //console.log(currentId);

        window.onmessage = function (e) {

            if(e.data=="toggle"){
                document.location="../index.html";
            }else{
            }

        }

        parent.postMessage("saisie", "*");
	</script>
</body>
</html>