
var angle = 360/animals.length;
var rotation = 0;
var index = 1;
var randomAnimalIndex;

var animal_id=[];
for(var i=0;i<16;i++){
    animal_id[i]=i;
}
animal_id=shuffleAffay(animal_id);
var animal_index=0;

var max_match=0;
var match_array=[];
var indexMatch=0;
var match_on=0;

animals.forEach((label, index) =>{
    $('#colorWheel').append("<span class=\"color"+index+"\"><img src=\"img/"+animals[index]+".png\"/></span>");
    var color = index%2 == 0 ? "rgb(254,249,230)" : "rgb(255,221,0)";
    $('#colorWheel span.color'+index).css({
        "-webkit-transform": "rotate("+rotation+"deg)",
        "-moz-transform": "rotate("+rotation+"deg)",
        "-ms-transform": "rotate("+rotation+"deg)",
        "-o-transform": "rotate("+rotation+"deg)",
        "transform": "rotate("+rotation+"deg)",
        "border-color": color+" transparent transparent transparent",
        "z-index": 50-index
    });
    index++;
    rotation += angle;
});

var spinAngle = 0;
var compensation = 0;
$("#wheelContainer, #triggerWhell").click(function(){




});

$(document).ready(function() {
    $('#animalSound').click(function(){
        //console.log("Jouer un son…");
        $("#audio")[0].play();
    });
    fakeRecognition();


});
//------------------------------------
//-- interaction
//-------------------------------------
var started = 0;
document.addEventListener("keydown", keyDownHandler, false);

function keyDownHandler(e) {
    var keyCode = e.keyCode;
    var keyOn = e.key;
    trigWheel();

}
function trigWheel(){
    $('#right-column').css("left","800px");
    $('#right-column').css("height","200px");

    $('#animalSound').attr('disabled', true); //Désactiver le bouton
    $('#title-up').html("...");
    $('#spectro-up').css("-moz-transition","unset");
    $('#spectro-up').css("-webkit-transition","unset");
    $('#spectro-up').css("transition","unset");
    $('#spectro-up').css("background-position-x","350px");
    $('#title-down').html("");
    $('#spectro-up').css("opacity",1);
    $('#spectro-down').css("opacity",0);
    $('#match').css("opacity",0);
    max_match=0;
    match_on=0;

    if(animal_index<16-1) {
        animal_index+=1;
    }else{
        animal_index=0;
    }
    randomAnimalIndex = animal_id[animal_index];//Math.floor((Math.random() * animals.length-1) + 1);
    console.log(animals[randomAnimalIndex]);

    spinAngle -= (360/animals.length)*randomAnimalIndex + compensation;
    compensation = 360 - (360/animals.length)*randomAnimalIndex;

    $('#colorWheel').css({
        "-webkit-transform": "rotate("+spinAngle+"deg)",
        "-moz-transform": "rotate("+spinAngle+"deg)",
        "transform": "rotate("+spinAngle+"deg)"
    });

    $('#colorWheel').one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",
        function(event) {


            $('#right-column').css("left","430px");

            //console.log("animation end…");
            $('#selectedAnimal').html(animals[randomAnimalIndex]);
            $('#title-column').html("Voici le bruit "+texts[randomAnimalIndex]+"&nbsp;: ");
            //!!!!!!!$("#audio")[0].src = "sounds/animals/"+animals[randomAnimalIndex]+".mp3";
            //$('#animalSound').attr('disabled', false); //Activer le bouton
            //!!!!!!!!$("#audio")[0].play();
            audios[randomAnimalIndex].play();
            $('#spectro-up').css("background-image","url('./img/_spectro-"+animals[randomAnimalIndex]+".jpeg')");
            $('#spectro-up').css("-moz-transition","background-position-x 2s ease-out");
            $('#spectro-up').css("-webkit-transition","background-position-x 2s ease-out");
            $('#spectro-up').css("transition","background-position-x 2s ease-out");
            $('#spectro-up').css("background-position-x","0px");

            //$("#audio")[0].addEventListener('ended', upToU);
            //setTimeout(upToU,2500);
        });
}
function upToU(){
   setTimeout(upNext,1000);

}
function upNext(){
    $('#right-column').css("height","460px");

    $('#title-down').html("Vous aussi faites le bruit "+texts[randomAnimalIndex]+" à voix haute&nbsp;: ");
    $('#spectro-down').css("opacity",1);

    if(started==0) {
        started=1;
        initialize();
    }else{
    }

    max_match=0;
    if(indexMatch<100-1) {
        indexMatch+=1;
    }else{
        indexMatch=0;
    }
    setTimeout(matchOn,2000);
}
function matchOn(){
    match_on=1;
    //$('#match').css("opacity",1);
    setTimeout(relanceroue,1000);
}
function relanceroue(){
    parent.postMessage("relanceroue", "*");
}
function fakeRecognition(){
    for(var i=0;i<100;i++){
        match_array[i]=parseInt(20+(Math.random()*25));
    }
    console.log(match_array);

}

function initialize() {

    const CVS = document.body.querySelector('canvas');
    const CTX = CVS.getContext('2d');
    const W = CVS.width = 60; //on rèlge ici la longeur du son
    const H = CVS.height = 4096;

    const ACTX = new AudioContext();
    const ANALYSER = ACTX.createAnalyser();

    ANALYSER.fftSize = 4096; //4096 //512

    navigator.mediaDevices
        .getUserMedia({ audio: true })
        .then(process);

    function process(stream) {
        const SOURCE = ACTX.createMediaStreamSource(stream);
        SOURCE.connect(ANALYSER);
        const DATA = new Uint8Array(ANALYSER.frequencyBinCount);
        const LEN = DATA.length;
        const h = H / LEN;
        const x = W - 1;
        CTX.fillStyle = 'hsl(280, 100%, 10%)';
        CTX.fillRect(0, 0, W, H);

        loop();

        function loop() {
            var maxVal=0;
            //if(count < 120){
            window.requestAnimationFrame(loop);
            //}

            //count++;

            let imgData = CTX.getImageData(1, 0, W - 1, H);
            CTX.fillRect(0, 0, W, H);
            CTX.putImageData(imgData, 0, 0);
            ANALYSER.getByteFrequencyData(DATA);
            for (let i = 0; i < LEN; i++) {
                let rat = DATA[i] / 255;
                let hue = Math.round((rat * 120) + 280 % 360);
                let sat = '100%';
                let lit = 10 + (70 * rat) + '%';
                CTX.beginPath();
                CTX.strokeStyle = `hsl(${hue}, ${sat}, ${lit})`;
                CTX.moveTo(x, H - (i * h));
                CTX.lineTo(x, H - (i * h + h));
                CTX.stroke();

                if(maxVal<DATA[i]) {
                    maxVal=DATA[i];
                }else{
                }
            }

            //console.log(maxVal);
            var valfinal=maxVal/10;
            if(valfinal<20) {
                valfinal=0;//0;parseInt(valfinal);
            }else{
                /*if(valfinal>maxLocal) {
                    //valfinal=maxLocal-(5*Math.random());
                    //valfinal=parseInt(valfinal*100)/100;
                    valfinal=maxLocal;
                }else{

                }*/
                valfinal=match_array[indexMatch];

            }
            if(match_on==1) {
                $('#match').css("opacity",1);
            }else{
            }

            max_match=Math.max(max_match,valfinal);
            $('#match').html("match "+max_match+"%");

        }
    }
}


function shuffleAffay (p_array)
{
    var n = p_array.length;
    var i = n;
    var temp;
    var p;

    while (i--) {
        p = Math.floor(Math.random()*n);
        temp = p_array[i];
        p_array[i] = p_array[p];
        p_array[p] = temp;
    }
    return p_array;
}

window.onmessage = function (e) {

    if(e.data=="lanceroue"){
        trigWheel();
    }else{
    }

}