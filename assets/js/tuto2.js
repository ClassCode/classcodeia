(function($){

    // Algorithme de classification : (true), featureExtractor (false)
    const KNN = true;

    // documentation ML5 :
    // - K-Nearest neighbours Classifier : https://learn.ml5js.org/docs/#/reference/knn-classifier
    // - FeatureExtractor Classifier : https://learn.ml5js.org/docs/#/reference/feature-extractor

    // SOURCES KNN :
    // https://github.com/ml5js/ml5-library/blob/development/src/KNNClassifier/index.js


    // Support pour le navigateur Microsoft Edge, version non-Chromium ( Microsoft Edge 44.18362.449.0 ):
    // 1. Un polyfill a été également ajouté pour le support de TextEncoding : https://github.com/inexorabletash/text-encoding
    // 2. Desactivation du passage par la carte graphique via WebGL
    if (document.documentMode || /Edge/.test(navigator.userAgent)) {
        _tfengine.ENV.set('WEBGL_PACK', false);
    }

    // Elements
    let $application = $('.tuto-ia-application');
    let $header  = $('#step-header');
    let $content = $('#step-contents');
    let $footer  = $('#step-footer');
    let $dataSetElement;

    // BaseURL : écrit dans le index.php via PHP
    let baseURL = $application.attr('data-baseURL');

    // Lang : ecrit dans le index.php via PHP
    let lang = $application.attr('data-lang');
    // Lang file extension used for subtitles
    let langExt = lang && lang === 'fr' ? '':`-${lang}`;
    let videoExt = langExt;

    // Chemins
    let dataFolder = baseURL + "/data";
    let datasetFolder = baseURL + "/datasets";
    let assetsFolder = baseURL + "/assets";

    // Tutoriel 1 :
    let appBaseRoute = baseURL + "/app/tuto2";
    let tutorialFolder = dataFolder + "/tuto2";

    // Templates HandleBars :
    Handlebars.templateFolder = tutorialFolder + '/templates/';
    Handlebars.templates = [];

    // Mise en cache des templates JSON des différents écrans pour ne pas les charger plusieurs fois
    let _tutorialJson;

    let currentAbsoluteRoute;
    let currentRelativeRoute;

    // Bibliothèque avec navigation
    let currentDatasetName;
    let currentDatasetCategory;

    // Nombre d'images dans la sélection
    const IMAGES_SELECTION_LENGTH = 10;
    const IMAGES_WEBCAM_LENGTH = 10;

    // Nombre d'images dans la librairie de test
    const IMAGES_PREDICTION_LENGTH = 40;

    // Mémorisation de sélection d'images (tableaux associatifs : clé = route relative )
    let _datasetSelections = [];
    let _datasetCategories = [];

    // Mémorisation des listes d'images de chaque catégorie  ( entrainement, prediction )
    let _datasetsJson;
    let _filteredDatasetsJson;

    // ML5 :
    let featureExtractor;
    let imageClassifier;

    // Webcam :
    let webcamVideo;



    // -------------------------------------------------------------------------------------------------------- //
    // FONCTIONS LIEES AU TUTORIEL
    // -------------------------------------------------------------------------------------------------------- //

    //
    // Evènements
    //

    // RQ : Les évènements souris sont détectés à la racine du DOM de l'application

    // Avant le chargement de la template :
    function beforeLoadingTemplateJsonForRoute( route ) {

        let templateParams = {};

        route = route.split(appBaseRoute).join('');
        switch(route) {

            case '/tester/etape1':
                closeWebcam();

                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto2/mp4/tuto2-activite1-vid1" + videoExt + ".mp4";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto2-activite1-vid1" + langExt + ".vtt";
                break;

            case '/tester/etape2':
                // Création d'un classifier d'images
                initPreTrainedClassifier();
                break;

            case '/tester/etape3':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto2/mp4/tuto2-activite1-vid2" + videoExt + ".mp4";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto2-activite1-vid2" + langExt + ".vtt";
                break;

            case '/experimenter/etape1':
                closeWebcam();

                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto2/mp4/tuto2-activite1-vid3" + videoExt + ".mp4";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto2-activite1-vid3" + langExt + ".vtt";

                // Suppression des catégories et sélections d'images précédemment définies
                clearRouteData('/experimenter/etape2' );
                clearRouteData('/experimenter/etape3' );

                break;

            case '/experimenter/etape2':
                // Catégorie 1 à insérer dans la template :
                templateParams.category1 = getImagesCategory( '/experimenter/etape2' );

                // Création d'un classifier d'images
                initFeaturesExtractionClassifier();
                break;

            case '/experimenter/etape3':
                // Catégorie 1 à insérer dans la template :
                templateParams.category1 = getImagesCategory( '/experimenter/etape2' );
                break;

            case '/experimenter/etape4':
            case '/experimenter/etape5':
            case '/experimenter/etape7':
                // Catégorie 1 et 2 à insérer dans la template :
                templateParams.category1 = getImagesCategory( '/experimenter/etape2' );
                templateParams.category2 = getImagesCategory( '/experimenter/etape3' );
                break;

            case '/experimenter/etape6':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto2/mp4/tuto2-activite1-vid4" + videoExt + ".mp4";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto2-activite1-vid4" + langExt + ".vtt";
                break;

            case '/experimenter/etape8':
            case '/experimenter/etape9':
                // Catégorie 1 et 2 à insérer dans la template :
                templateParams.category1 = getImagesCategory( '/experimenter/etape8' );
                templateParams.category2 = getImagesCategory( '/experimenter/etape9' );
                break;

            case '/experimenter/etape10':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto2/mp4/tuto2-activite1-vid5" + videoExt + ".mp4";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto2-activite1-vid5" + langExt + ".vtt";
                break;

            case '/creer/etape1':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto2/mp4/tuto2-activite1-vid6" + videoExt + ".mp4";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto2-activite1-vid6" + langExt + ".vtt";

                // Suppression des catégories et sélections d'images précédemment définies
                clearRouteData('/creer/etape2' );
                clearRouteData('/creer/etape3' );

                break;

            case '/creer/etape2':
                // Catégorie 1 et 2 à insérer dans la template :
                templateParams.category1 = getImagesCategory( '/creer/etape2' ) || 'catégorie 1';
                templateParams.category2 = getImagesCategory( '/creer/etape3' ) || 'catégorie 2';

                // Création d'un classifier d'images
                initFeaturesExtractionClassifier();

                break;

            case '/creer/etape3':
                // Catégorie 1 et 2 à insérer dans la template :
                templateParams.category1 = getImagesCategory( '/creer/etape2' );
                templateParams.category2 = getImagesCategory( '/creer/etape3' );
                break;

            case '/conclure/etape1':
                closeWebcam();

                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto2/mp4/tuto2-activite1-vid7" + videoExt + ".mp4";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto2-activite1-vid7" + langExt + ".vtt";
                break;

            case '/credits' :
                closeWebcam();
                break;

            case '/tester_entrainement' :
                // Création d'un classifier d'images
                initFeaturesExtractionClassifier();
                break;

            case '/tester_prediction' :
                // Création d'un classifier d'images
                initPreTrainedClassifier();
        }

        return templateParams;
    }

    // Une fois la template chargée et insérée dans le DOM :
    function afterInsertingTemplateForRoute( route ) {

        route = route.split(appBaseRoute).join('');

        // console.log('Templated Loaded', route);

        let dataCompleteForCurrentStep;
        let infosCategory1, infosCategory2, pathsCategory1;
        let categoryFemme, categoryHomme;

        let mergeFolders = function(datasetsJson) {

            // On fusionne tous les sous-dossiers dans un dataset :
            let filteredData = [];
            let filteredFiles = [];

            let i, datasetsCount = datasetsJson.length, dataset;
            for (i = 0; i < datasetsCount; i++) {

                dataset = datasetsJson[i];

                let j, m = dataset.files.length, fileInfo;
                for (j = 0; j < m; j++) {
                    fileInfo = dataset.files[j];
                    filteredFiles.push({
                        name: fileInfo.name,
                        path: fileInfo.path,
                        datasetname: dataset.folder
                    });
                }
            }

            filteredData.push({
                files: filteredFiles
            });

            return filteredData;
        };

        // Ajoute une classe 'route_....' (sert à l'accueil pour la couleur de fond)
        $('.step-contents').attr('class', 'step-contents route' + route.split('/').join('_'));

        switch(route) {

            case '/':
               break;

            case '/tester/etape2':

                // Chargement de la bibliothèque (prediction)
                // On présente toutes les images de tous les sous-dossiers

                checkForNavigableDatasets( mergeFolders );

                break;

            case '/experimenter/etape2':

                // Initialisation de la sélection d'images de la première catégorie
                resetOrRestoreImagesSelection( '/experimenter/etape2' );

                checkForNavigableDatasets( mergeFolders );

                break;

            case '/experimenter/etape3':

                dataCompleteForCurrentStep = false;

                // Affichage de la sélection d'images de la première catégorie
                if ( restoreImagesSelection( '/experimenter/etape2', $('.category1-images-grid') ))
                {
                    dataCompleteForCurrentStep = true;

                    // Infos sur les images choisies à l'étape précédente pour la première catégorie
                    infosCategory1 = getInfosFromStoredImageSelectionForRoute('/experimenter/etape2');
                    pathsCategory1 = infosCategory1.paths;

                    // Affichage de la sélection d'images de la seconde catégorie
                    resetOrRestoreImagesSelection( '/experimenter/etape3' );

                    checkForNavigableDatasets(function (datasetsJson) {

                        // On fusionne tous les sous-dossiers dans un dataset :
                        let filteredData = [];
                        let filteredFiles = [];

                        let i, datasetsCount = datasetsJson.length, dataset;
                        for (i = 0; i < datasetsCount; i++) {

                            dataset = datasetsJson[i];

                            let j, m = dataset.files.length, fileInfo;
                            for (j = 0; j < m; j++) {
                                fileInfo = dataset.files[j];
                                if ( pathsCategory1.indexOf(fileInfo.path) === -1) {
                                    filteredFiles.push({
                                        name: fileInfo.name,
                                        path: fileInfo.path,
                                        datasetname: dataset.folder
                                    });
                                }
                            }
                        }

                        filteredData.push({
                            files: filteredFiles
                        });

                        return filteredData;
                    });
                }

                if (! dataCompleteForCurrentStep ) {
                    // Données incomplète pour faire cette étape
                    // On renvoie vers l'accueil
                    backHome();
                }

                break;

            case '/experimenter/etape4':

                dataCompleteForCurrentStep = false;

                // Affichage de la sélection d'images de la première catégorie
                if ( restoreImagesSelection( '/experimenter/etape2', $('.category1-images-grid') ))
                {
                    // Affichage de la sélection d'images de la seconde catégorie
                    if ( restoreImagesSelection( '/experimenter/etape3', $('.category2-images-grid') ))
                    {
                        dataCompleteForCurrentStep = true;
                    }
                }

                if (! dataCompleteForCurrentStep ) {
                    // Données incomplète pour pouvoir faire cette étape
                    // On renvoie vers l'accueil
                    backHome();
                } else {
                    // Chargement de la bibliothèque :
                    checkForNavigableDatasets();
                }

                break;

            case '/experimenter/etape5':
            case '/experimenter/etape7':

                dataCompleteForCurrentStep = false;

                // Affichage de la sélection d'images de la première catégorie
                if ( restoreImagesSelection( '/experimenter/etape2', $('.category1-images-grid') ))
                {
                    // Affichage de la sélection d'images de la seconde catégorie
                    if ( restoreImagesSelection( '/experimenter/etape3', $('.category2-images-grid') ))
                    {
                        dataCompleteForCurrentStep = true;
                    }
                }

                infosCategory1 = getInfosFromStoredImageSelectionForRoute('/experimenter/etape2');
                infosCategory2 = getInfosFromStoredImageSelectionForRoute('/experimenter/etape3');
                dataCompleteForCurrentStep = dataCompleteForCurrentStep && infosCategory1 && infosCategory1.datasetName && infosCategory2 && infosCategory2.datasetName;

                if (! dataCompleteForCurrentStep ) {
                    // Données incomplète pour pouvoir faire cette étape
                    // On renvoie vers l'accueil
                    backHome();

                } else {

                    categoryFemme = getImagesCategory('/experimenter/etape2');
                    categoryHomme = getImagesCategory('/experimenter/etape3');

                    // Chargement de la bibliothèque (prédiction) :
                    checkForPredictionDatasets(categoryFemme, categoryHomme, function (datasetsJson) {

                        // On fusionne tous les sous-dossiers dans un dataset :
                        let filteredData = [];
                        let filteredFiles = [];

                        let i, datasetsCount = datasetsJson.length, dataset;
                        for (i = 0; i < datasetsCount; i++) {
                            dataset = datasetsJson[i];
                            if ((categoryFemme) || (categoryHomme)) {
                                let j, m = dataset.files.length, fileInfo;
                                for (j = 0; j < m; j++) {
                                    fileInfo = dataset.files[j];
                                    filteredFiles.push({
                                        name: fileInfo.name,
                                        path: fileInfo.path,
                                        datasetname: dataset.folder
                                    });
                                }
                            }
                        }

                        // On mélange et on ne conserve qu'un nombre limité d'images
                        filteredData.push({
                            files: shuffleAffay(filteredFiles).slice(0, IMAGES_PREDICTION_LENGTH)
                        });

                        return filteredData;
                    });
                }

                break;

            case '/experimenter/etape8':

                dataCompleteForCurrentStep = false;

                // Affichage de la sélection d'images de la première catégorie
                if ( restoreImagesSelection( '/experimenter/etape8', $('.category1-images-grid') ))
                {
                    // Affichage de la sélection d'images de la seconde catégorie
                    if ( restoreImagesSelection( '/experimenter/etape9', $('.category2-images-grid') ))
                    {
                        dataCompleteForCurrentStep = true;
                    }
                }

                if (! dataCompleteForCurrentStep ) {
                    // Données incomplète pour pouvoir faire cette étape
                    // On renvoie vers l'accueil
                    backHome();

                } else {

                    categoryFemme = getImagesCategory( '/experimenter/etape2' );
                    categoryHomme = getImagesCategory( '/experimenter/etape3' );

                    // Chargement de la bibliothèque (prédiction) :
                    checkForPredictionDatasets(categoryFemme, categoryHomme, function (datasetsJson) {

                        // On fusionne tous les sous-dossiers dans un dataset :
                        let filteredData = [];
                        let filteredFiles = [];

                        let i, datasetsCount = datasetsJson.length, dataset;

                        for (i = 0; i < datasetsCount; i++)
                        {
                            dataset = datasetsJson[i];
                            if ((categoryFemme) || (categoryHomme))
                            {
                                let j, m = dataset.files.length, fileInfo;
                                for (j = 0; j < m; j++) {
                                    fileInfo = dataset.files[j];
                                    filteredFiles.push({
                                        name: fileInfo.name,
                                        path: fileInfo.path,
                                        datasetname: dataset.folder
                                    });
                                }
                            }
                        }

                        // On mélange et on ne conserve qu'un nombre limité d'images
                        filteredData.push({
                            files: shuffleAffay(filteredFiles).slice(0, IMAGES_PREDICTION_LENGTH)
                        });

                        return filteredData;
                    });

                    if (! webcamVideo )
                    {
                        webcamVideo = initWebcam( function() {
                            $('.snapshot-button').removeClass('hidden');
                        });
                    }
                    else
                    {
                        $('.snapshot-button').removeClass('hidden');
                        webcamVideo.play();
                    }

                    $('.parent-webcam').append( webcamVideo );
                }

                break;

            case '/experimenter/etape9':

                dataCompleteForCurrentStep = false;

                // Affichage de la sélection d'images de la première catégorie
                if ( restoreImagesSelection( '/experimenter/etape8', $('.category1-images-grid') ))
                {
                    // Affichage de la sélection d'images de la seconde catégorie
                    if ( restoreImagesSelection( '/experimenter/etape9', $('.category2-images-grid') ))
                    {
                        dataCompleteForCurrentStep = true;
                    }
                }

                if (! dataCompleteForCurrentStep ) {
                    // Données incomplètes pour pouvoir faire cette étape
                    // On renvoie vers l'accueil
                    backHome();

                } else {

                    // Ajout du bouton Rejouer

                    let replayHtml = '';
                    replayHtml += '<li class="navigation-route" data-route="/experimenter/etape2">';
                    replayHtml += '<a class="navigation-link replay-link" href="/experimenter/etape2">' + i18n('Rejouer') + '</a></li>';

                    $(replayHtml).insertBefore('.navigation-route.next-link');

                    categoryFemme = "Femme";
                    categoryHomme = "Homme";

                    // Chargement de la bibliothèque (prédiction) :
                    checkForPredictionDatasets(categoryFemme, categoryHomme, function (datasetsJson) {

                        // On fusionne tous les sous-dossiers dans un dataset :
                        let filteredData = [];
                        let filteredFiles = [];

                        let i, datasetsCount = datasetsJson.length, dataset;

                        for (i = 0; i < datasetsCount; i++) {
                            dataset = datasetsJson[i];
                            if ((categoryFemme) || (categoryHomme)) {
                                let j, m = dataset.files.length, fileInfo;
                                for (j = 0; j < m; j++) {
                                    fileInfo = dataset.files[j];
                                    filteredFiles.push({
                                        name: fileInfo.name,
                                        path: fileInfo.path,
                                        datasetname: dataset.folder
                                    });
                                }
                            }
                        }

                        // On mélange et on ne conserve qu'un nombre limité d'images
                        filteredData.push({
                            files: shuffleAffay(filteredFiles).slice(0, IMAGES_PREDICTION_LENGTH)
                        });

                        return filteredData;
                    });

                }

                break;

            case '/creer/etape2':

                resetOrRestoreImagesSelection( '/creer/etape2', IMAGES_SELECTION_LENGTH, $('.category1-images-grid') );
                resetOrRestoreImagesSelection( '/creer/etape3', IMAGES_SELECTION_LENGTH, $('.category2-images-grid') );

                // La première fois que l'on affiche cette étape, pas de boutons Modifier et Valider
                // Une fois rempli la première catégorie, on affiche Valider
                // Une fois validé, on affiche le bouton "Modifier"

                // La première fois que l'on valide, on active la catégorie 2

                // Si on revient de l'étape 3, on affiche les 2 sélections et les boutons "Modifier"

                console.log( "isWebcamImagesComplete 1", isWebcamImagesComplete($('.category1-images-grid')));
                console.log( "isWebcamImagesComplete 2", isWebcamImagesComplete($('.category2-images-grid')));

                if ( isWebcamImagesComplete($('.category1-images-grid')) && isWebcamImagesComplete($('.category2-images-grid')) )
                {
                    // Retour de l'étape 3 : affichage des boutons Modifier
                    $('.modify-category1-button').removeClass('hidden');
                    $('.modify-category2-button').removeClass('hidden');

                    // Les bibliothèques ne sont pas modifiables avant d'avoir cliquer sur "Modifier"
                    $('.category-images-grid').removeClass('selected-images-grid');
                    $('.category2-images-grid').removeClass('inactive');
                    $('.instructions-panel-header-category2').removeClass('inactive');
                }

                if (! webcamVideo )
                {
                    webcamVideo = initWebcam( function() {
                        $('.snapshot-button').removeClass('hidden');
                    });
                }
                else
                {
                    $('.snapshot-button').removeClass('hidden');
                    webcamVideo.play();
                }

                $('.parent-webcam').append( webcamVideo );

                break;

            case '/creer/etape3':

                dataCompleteForCurrentStep = false;

                // Affichage de la sélection d'images de la première catégorie
                if ( restoreImagesSelection( '/creer/etape2', $('.category1-images-grid') ))
                {
                    // Affichage de la sélection d'images de la seconde catégorie
                    if ( restoreImagesSelection( '/creer/etape3', $('.category2-images-grid') ))
                    {
                        dataCompleteForCurrentStep = true;
                    }
                }

                if (! dataCompleteForCurrentStep ) {
                    // Données incomplètes pour pouvoir faire cette étape
                    // On renvoie vers l'accueil
                    backHome();

                } else {

                    // Webcam pour la prédiction

                    if (! webcamVideo )
                    {
                        webcamVideo = initWebcam( function() {
                            $('.snapshot-button').removeClass('hidden');
                        });
                    }
                    else
                    {
                        $('.snapshot-button').removeClass('hidden');
                        webcamVideo.play();
                    }

                    $('.parent-webcam').append( webcamVideo );

                }

                break;

            case '/tester_entrainement' :

                checkForNavigableDatasets( mergeFolders );
                break;

            case '/tester_prediction' :

                // On présente toutes les images de tous les sous-dossiers
                checkForNavigableDatasets( mergeFolders );
                break;
        }
    }

    // Si l'utilisateur clique sur un élement d'interface
    $application.on('click', function(e) {

        let tagName = e.target.tagName.toLowerCase();
        let target = $(e.target);

        if ((tagName == "label") || (tagName == "input") || target.hasClass("checkmark"))  {

            // Cas des inputs, labels, ... où l'évènement doit conserver son fonctionnement normal
            if  (target.hasClass("checkmark"))
            {
                var liTarget = target.closest('.with-image-in-span');
                liTarget.removeClass().removeAttr('style').empty();

                if (liTarget.length &&  ! $('.validate-selection-button.active').hasClass("hidden")) {
                    $('.validate-selection-button.active').addClass("hidden");
                }
            }

        } else {

            // Sinon :

            // Liens basiques (credits)
            if (target.hasClass("basic-link"))
            {
                return;
            }

            e.preventDefault();

            let message, $li;

            if (target.hasClass("navigation-link"))
            {
                if (target.hasClass("replay-link"))
                {
                    clearRouteData('/experimenter/etape2');
                    clearRouteData('/experimenter/etape3');

                    clearRouteData('/experimenter/etape8');
                    clearRouteData('/experimenter/etape9');
                }

                let navigationLi = target.closest('.navigation-route');
                let navigationRoute = navigationLi.data('route');

                $.router.set({
                    route: appBaseRoute + navigationRoute
                });
            }
            else if (target.hasClass("video-parent"))
            {
                let videoElement = target.find('video');
                playVideo(videoElement);
            }
            else if ( target.hasClass("video-background") || target.hasClass("video-infos"))
            {

                let videoParent = target.closest('.video-parent');
                let videoElement = videoParent.find('video');
                playVideo(videoElement);

            }
            else if (target.hasClass("previous-dataset"))
            {
                previousDataset();
            }
            else if (target.hasClass("next-dataset"))
            {
                nextDataset();
            }
            else if (target.hasClass("dataset-image"))
            {
                // Click sur une image de la bibliothèque :
                let imageTarget = $(e.target);

                // On vérifie que la bibliothèque a la propriété cliquable
                let clickableDataset = imageTarget.closest('.clickable-images-grid');
                if ( clickableDataset.length )
                {
                    //
                    // Cas de click sur une image d'une bibliothèque pour l'ajouter à la sélection
                    //

                    let parentOfImageForPrediction, added;

                    $('.dataset-image', clickableDataset).removeClass('last-selected');
                    imageTarget.addClass('last-selected');

                    switch(currentRelativeRoute) {

                        case '/experimenter/etape2' :
                        case '/experimenter/etape3' :

                            let $datasetNamesSelector = $('.dataset-names-selector');
                            let category = $datasetNamesSelector.val();

                            imageTarget.find('img').data('datasetname', category);

                            // Sélection d'image d'une même catégorie
                            added = addImageToSelection( imageTarget );

                            // On sauvegarde la catégorie de l'image ajoutée
                            saveImagesCategory( currentRelativeRoute, category );

                            // Si la sélection d'images est complète, on affiche le bouton :
                            if (added && isImagesSelectionComplete()) {
                                $('.validate-selection-button.active').removeClass('hidden');
                            }

                            break;

                        case '/experimenter/etape8' :
                        case '/creer/etape2' :

                            let $activeCategory = $('.instructions-panel-left-header input').not('.hidden');
                            if ($activeCategory.length) {

                                // Sélection d'image d'une même catégorie
                                imageTarget = $(e.target);
                                added = addImageToSelection( imageTarget, false );

                                // Si la sélection d'images est complète, on affiche le bouton :
                                if (added && isImagesSelectionComplete()) {
                                    $('.validate-selection-button.active').removeClass('hidden');
                                }
                            }

                            break;

                        case '/tester_prediction' :
                        case '/tester/etape2' :
                        case '/experimenter/etape5':
                        case '/experimenter/etape7':
                        case '/experimenter/etape9':
                        case '/creer/etape3' :

                            // Sélection d'une seule image pour une prédiction

                            parentOfImageForPrediction = $('.image-for-prediction');
                            parentOfImageForPrediction.empty();

                            imageTarget = $(e.target);
                            imageTarget.clone().appendTo( parentOfImageForPrediction );

                            $('.feature-predict-button').removeClass('hidden');
                            $('.prediction-result').empty();

                            break;
                    }
                }
                else if ( imageTarget.closest('.selected-images-grid').length )
                {
                    // Suppression d'une image du dataset

                    // On vérifie si cette image n'est pas Sélectionnée dans la bibliothèque
                    let imageURL = $('img', imageTarget).attr('src');
                    var gridElement = $('.clickable-images-grid');
                    $('li', gridElement).each(function(no, item) {
                        let $li = $(item);
                        if ($('img', $li).attr("src") === imageURL ) {
                            $li.removeClass("selected");
                        }
                    });

                    var liTarget = target.closest('.with-image-in-span');
                    if (liTarget.length === 0) {
                        liTarget = target;
                    }
                    liTarget.removeClass().removeAttr('style').empty();

                    if (liTarget.length && !$('.validate-selection-button.active').hasClass("hidden")) {
                        $('.validate-selection-button.active').addClass("hidden");
                    }
                }
            }
            else if (target.hasClass("validate-selection-button"))
            {
                let categoryName;

                switch(currentRelativeRoute) {

                    case '/experimenter/etape2' :

                        // Validation de sélection d'images de la première catégorie

                        // On mémorise la sélection pour les écrans suivants ou pour un rechargement de la page
                        saveImagesSelection( currentRelativeRoute );

                        // On se rend à l'étape suivante
                        goRoute('/experimenter/etape3');

                        break;

                    case '/experimenter/etape3' :

                        // Validation de sélection d'images de la première catégorie

                        // On mémorise la sélection pour les écrans suivants ou pour un rechargement de la page
                        saveImagesSelection( currentRelativeRoute );

                        // On se rend à l'étape suivante
                        goRoute('/experimenter/etape4');

                        break;

                    case '/experimenter/etape8' :

                        if (target.hasClass("validate-category1-button")) {
                            stopModifyingCategory1('/experimenter/etape8');
                        } else if (target.hasClass("validate-category2-button")) {
                            stopModifyingCategory2('/experimenter/etape9');
                        }

                        break;

                    case '/creer/etape2' :

                        if (target.hasClass("validate-category1-button")) {

                            // RQ : La première fois que l'on valide la catégorie 1,
                            // on active la seconde catégorie, mais sans afficher les boutons Modifier/Valider

                            // Si la seconde sélection d'images n'a jamais été sauvegardées, c'est qu'on n'est pas dans le cycle modifier/valider
                            // mais dans la première validation de la catégorie 1
                            var isFirstValidationForCategory1 = getInfosFromStoredImageSelectionForRoute('/creer/etape3').datasetName === undefined;

                            stopModifyingCategory1('/creer/etape2', isFirstValidationForCategory1);

                        } else if (target.hasClass("validate-category2-button")) {
                            stopModifyingCategory2('/creer/etape3');
                        }

                        break;
                }
            }
            else if (target.hasClass("modify-selections-button"))
            {
                switch(currentRelativeRoute) {

                    case '/experimenter/etape7' :

                        // On clone les données de l'étape 2-3 pour les étapes 8-9

                        // - Les images :
                        saveImagesSelection( '/experimenter/etape8', $('.category1-images-grid'));
                        saveImagesSelection( '/experimenter/etape9', $('.category2-images-grid'));

                        // - Les noms de catégorie :
                        saveImagesCategory( '/experimenter/etape8', $('.instructions-panel-header-category1 h4').html());
                        saveImagesCategory( '/experimenter/etape9', $('.instructions-panel-header-category2 h4').html());

                        $.router.set({
                            route: appBaseRoute + '/experimenter/etape8'
                        });

                        break;

                    case '/experimenter/etape8' :

                        if (target.hasClass("modify-category1-button")) {
                            startModifyingCategory1( getInfosFromStoredImageSelectionForRoute('/experimenter/etape8') );
                        }
                        else if (target.hasClass("modify-category2-button"))
                        {
                            startModifyingCategory2( getInfosFromStoredImageSelectionForRoute('/experimenter/etape9') );
                        }

                        break;

                    case '/experimenter/etape9' :

                        $.router.set({
                            route: appBaseRoute + '/experimenter/etape8'
                        });

                        break;

                    case '/creer/etape2' :

                        if (target.hasClass("modify-category1-button")) {
                            startModifyingCategory1( getInfosFromStoredImageSelectionForRoute('/creer/etape2') );
                        }
                        else if (target.hasClass("modify-category2-button"))
                        {
                            startModifyingCategory2( getInfosFromStoredImageSelectionForRoute('/creer/etape3') );
                        }

                        break;

                    case '/creer/etape3' :

                        $.router.set({
                            route: appBaseRoute + '/creer/etape2'
                        });

                        break;
                }
            }
            else if (target.hasClass("train-selections-button"))
            {
                switch(currentRelativeRoute) {

                    case '/tester_entrainement' :

                        // On doit ajouter les images au classifier
                        addImagesToImagesClassifier( $('.dataset-images-grid img'), !KNN, function() {

                            imageClassifier.save('pretrained_tuto2.json');

                        });

                    break;

                    case '/experimenter/etape4' :

                        // On doit ajouter les images au classifier
                        addImagesToImagesClassifier( $('.category-images-grid img'), !KNN, function() {

                            // On se rend à l'étape suivante
                            goRoute('/experimenter/etape5');

                        });

                        break;

                    case '/experimenter/etape8' :

                        initFeaturesExtractionClassifier();

                        // On doit ajouter les images au classifier
                        addImagesToImagesClassifier( $('.category-images-grid img'), !KNN, function() {

                            // On se rend à l'étape suivante
                            goRoute('/experimenter/etape9');

                        });

                        break;

                    case '/creer/etape2' :

                        initFeaturesExtractionClassifier();

                        // On doit ajouter les images au classifier
                        addImagesToImagesClassifier( $('.category-images-grid img'), !KNN, function() {

                            // On se rend à l'étape suivante
                            goRoute('/creer/etape3');

                        });

                        break;

                }
            }
            else if (target.hasClass("feature-predict-button"))
            {
                $('.prediction-result').html('<p class="small">' + i18n('Analyse en cours...') + '</p>');

                setTimeout( function() {

                    // Image à analyser :
                    let imageToPredict = $('.image-for-prediction').find('img')[0];

                    predictImageWithClassifier( imageToPredict,  function(err, results) {

                        if (err) {
                            message = "Erreur !";
                        } else if (results.length === 0) {
                            message = "Aucun résultat !";
                        } else {

                            let firstResult, secondResult;
                            let firstResultLabel, secondResultLabel;
                            let firstResultConfidence, secondResultConfidence;
                            let label;
                            let nbsp = String.fromCharCode(160);

                            if (KNN)
                            {
                                var confidencesForSorting = [];
                                for (label in results.confidencesByLabel) {
                                    confidencesForSorting.push(
                                        { label: i18n(label), confidence: Math.floor( results.confidencesByLabel[label] * 100) }
                                    );
                                }

                                confidencesForSorting.sort( function(a, b){
                                    return a.confidence > b.confidence ? -1 : +1;
                                });

                                message = "";

                                var i, n = confidencesForSorting.length, confidencesObj;


                                for(i=0;i<n;i++){
                                    confidencesObj = confidencesForSorting[i];
                                    if(confidencesObj.confidence > 0) {
                                        message += i === 0 ? '<p class="large">'  : '<p class="small">';
                                        message += confidencesObj.label + " : " + confidencesObj.confidence + nbsp + "%" + '</p>';
                                    }
                                }
                            }
                            else
                            {
                                firstResult = results[0];
                                firstResultLabel = i18n(firstResult.label);
                                firstResultConfidence = Math.floor( firstResult.confidence * 100);

                                secondResult = results[1];
                                secondResultLabel = i18n(secondResult.label);
                                secondResultConfidence = Math.floor( secondResult.confidence * 100);

                                message = "";
                                message += '<p class="large">' + firstResultLabel + " : " + firstResultConfidence + nbsp + "%" + '</p>';
                                message += '<p class="small">' + secondResultLabel + " : " + secondResultConfidence + nbsp + "%" + '</p>';
                           }

                        }

                        $('.prediction-result').html( message );

                    });

                }, 100);

            }
            else if  (target.hasClass("snapshot-button"))
            {
                switch(currentRelativeRoute) {

                    case '/creer/etape2' :

                        $li = getVideoSnapShotWrappedAsLi();
                        let added = addImageToSelection( $li );

                        console.log( added, isImagesSelectionComplete() );

                        if (added && isImagesSelectionComplete()) {
                            $('.validate-selection-button.active').removeClass('hidden');
                        }


                        break;

                    case '/creer/etape3' :

                        // Sélection d'une seule image pour une prédiction

                        parentOfImageForPrediction = $('.image-for-prediction');
                        parentOfImageForPrediction.empty();

                        $li = getVideoSnapShotWrappedAsLi();
                        $li.appendTo( parentOfImageForPrediction );

                        $('.feature-predict-button').removeClass('hidden');
                        $('.prediction-result').empty();

                        break;

                    default:

                        // Snapshot de la webcam pour créer la bibliothèque d'image
                        if (0) {
                            $li = getVideoSnapShotWrappedAsLiWithCheckbox();
                        } else {
                            $li = getVideoSnapShotWrappedAsLi();
                        }

                        addImageToSelection($li);

                        // Si la sélection d'images est complète, on affiche le bouton :
                        if (isImagesSelectionComplete()) {

                            $('.validate-selection-button.active').removeClass('hidden');
                        }

                        // Sauvegarde pour les étapes suivantes
                        saveImagesSelection(currentRelativeRoute);
                }

            }
            else if  (target.hasClass("snapshot-for-test-button"))
            {
                // Snapshot de la webcam pour tester les deux catégories d'images issues de la webcam
                parentOfImageForPrediction = $('.image-for-prediction');
                parentOfImageForPrediction.empty();

                $li = getVideoSnapShotWrappedAsLi();
                $li.appendTo( parentOfImageForPrediction );

                $('.feature-predict-button').removeClass('hidden');

            }
        }
    });

    // Si l'utilisateur choisit une option d'un menu déroulant
    $application.on('change', function(e) {

        let tagName = e.target.tagName.toLowerCase();
        let target = $(e.target);

        if ((tagName == "label") || (tagName == "input"))  {
            // Cas des inputs, labels, ... où l'évènement doit conserver son fonctionnement normal

            if (e.target.files) {

                var file = e.target.files[0];
                var imageType = /image.*/;

                if (file.type.match(imageType)) {

                    var reader = new FileReader();

                    if ( target.hasClass("dataset-file-upload"))
                    {
                        // Import de l'image sélectionnée (sur le bureau ou un dossier) dans le DOM
                        // Destination : une galerie d'image

                        reader.onload = function(e) {

                            var img = new Image();
                            img.src = reader.result;

                            let li = document.createElement('li');
                            li.appendChild(img);

                            let $li = $(li);
                            $li.addClass('with-image-in-span');

                            let withCheckbox = false;

                            if (withCheckbox)
                            {
                                $li.addClass('with-checkbox');
                                $li.append('<span class="image">');

                                let $span = $li.find('span');
                                $span.addClass( 'dataset-image' );
                                $span.css('background-image', 'url(' +img.src+ ')' );

                                let $img  = $li.find('img');
                                $img.addClass("hidden-dataset-image");

                                // Checkbox
                                $li.append('<label class="styled-checkbox"><input type="checkbox" checked="checked" /><span class="checkmark"></span></label>');
                            }
                            else
                            {
                                $li.append('<span class="image">');

                                let $span = $li.find('span');
                                $span.addClass( 'dataset-image' );
                                $span.css('background-image', 'url(' +img.src+ ')' );

                                let $img  = $li.find('img');
                                $img.addClass("hidden-dataset-image");
                            }

                            var added = addImageToSelection( $li );

                            // Si la sélection d'images est complète, on affiche le bouton :
                            if (added && isImagesSelectionComplete()) {
                                $('.validate-selection-button.active').removeClass('hidden');
                            }
                        };

                        reader.readAsDataURL(file);

                    }
                    else if ( target.hasClass("prediction-file-upload"))
                    {
                        // Import de l'image sélectionnée (sur le bureau ou un dossier) dans le DOM
                        // Destination : l'image à prédire

                        reader.onload = function(e) {

                            var img = new Image();
                            img.src = reader.result;

                            let li = document.createElement('li');
                            li.appendChild(img);

                            let $li = $(li);
                            $li.addClass('with-image-in-span');
                            $li.append('<span class="image">');

                            let $span = $li.find('span');
                            $span.addClass( 'dataset-image' );
                            $span.css('background-image', 'url(' +img.src+ ')' );

                            let $img  = $li.find('img');
                            $img.addClass("hidden-dataset-image");

                            parentOfImageForPrediction = $('.image-for-prediction');
                            parentOfImageForPrediction.empty();

                            $li.appendTo( parentOfImageForPrediction );

                            $('.feature-predict-button').removeClass('hidden');

                        };

                        reader.readAsDataURL(file);
                    }

                } else {
                    // File not supported!
                }
            }

        } else {

            // Sinon :

            e.preventDefault();

            if ( target.hasClass("dataset-names-selector"))
            {
                switch(currentRelativeRoute) {

                    case '/experimenter/etape2' :
                    case '/experimenter/etape3' :

                        // Expérimenter : sélection de la première bibliothèque
                        currentDatasetName = target.val();

                        // Chargement de la bibliothèque d'images choisie dans le selecteur
                        displayDataset( $dataSetElement, currentDatasetCategory, currentDatasetName );

                        // On vide la sélection d'images
                        resetImagesSelection();

                        break;
                }
            }
        }
    });



    // -------------------------------------------------------------------------------------------------------- //
    // FONCTIONS GENERALES
    // -------------------------------------------------------------------------------------------------------- //

    //
    // Router
    // https://github.com/scssyworks/silkrouter/tree/feature/ver2
    //

    $.route(function (data) {

        currentAbsoluteRoute = data.route;

        let relativeRoute = currentAbsoluteRoute.split(appBaseRoute).join('');

        // console.log("Route absolue", currentAbsoluteRoute, "Route relative", relativeRoute);

        if (_tutorialJson === undefined) {
            loadChapitresJson( function( json ) {
                currentRelativeRoute = updateContentWithRoute( _tutorialJson = json, relativeRoute);
            });
        } else {
            currentRelativeRoute = updateContentWithRoute( _tutorialJson, relativeRoute);
        }
     });

    // init translations then init route
    initTutorielTranslations(baseURL, lang, tutorialFolder, function() {
        $.router.init();
    });



    // Retour forcé à l'accueil : par exemple, si l'utilisateur recharge une étape intermédiaire
    function backHome() {
        goRoute('/');
    }

    function goRoute( route ) {
        $.router.set({
            route: appBaseRoute + route
        });
    }


    //
    // Chargement du JSON des templates
    //

    function loadChapitresJson( successCallback ) {
        getTutorielJSON( tutorialFolder + '/json/chapitres.json', function( json ) {
            if(successCallback) {
                successCallback( json );
            }
        });
    }


    //
    // Affichage de la template de la route
    //

    function updateContentWithRoute( tutoJson, route ) {

        // console.log("updateContentWithRoute", tutoJson, route);

        // La valeur de la route passée en paramètre peut être modifié ( ex : chapitre )
        let contentDescription = getStepDescriptionForRoute(tutoJson, route);
        let relativeRoute = contentDescription.step.route;

        // console.log("relativeRoute", contentDescription, relativeRoute);

        // Préparation des données à injecter dans la template
        let templateParams = beforeLoadingTemplateJsonForRoute( relativeRoute );

        // Chargement de la template
        displayRoute( tutoJson, appBaseRoute, relativeRoute, $header, $content, $footer, contentDescription, templateParams );

        // Application des régles liés à la template après chargement
        afterInsertingTemplateForRoute(relativeRoute);

        return relativeRoute;
    }


    //
    // Datasets ( bibliothèque d'images )
    //

    // Template HTML d'une image : synchrone, définie dans la page index.php
    let datasetTemplateSource   = $("#dataset-template").html();
    let template = Handlebars.compile(datasetTemplateSource);

    // Dataset :
    function checkForNavigableDatasets( datasetFilterFunction, successCallback ) {

        $dataSetElement = $('.navigable-images-grid');
        if ( $dataSetElement.length )
        {
            let datasetCategory = $dataSetElement.data('category');
            if ((_datasetsJson === undefined) || (_datasetsJson[datasetCategory] === undefined)) {

                // console.log("Appel à l'API des images");

                // On interroge l'API pour récupérer la liste des sous-dossiers et leurs contenus pour une catégorie ( entrainement, prediction )
                loadJsonDataset( datasetCategory, function( json ) {

                    if (_datasetsJson === undefined) {
                        _datasetsJson = [];
                    }

                    // On stocke le retour de l'API pour un dossier ( entrainement, prediction)
                    _datasetsJson[datasetCategory] = json.datasets;

                    if (datasetFilterFunction) {
                        _filteredDatasetsJson = datasetFilterFunction( _datasetsJson[datasetCategory] );
                    } else {
                        _filteredDatasetsJson = _datasetsJson[datasetCategory];
                    }

                    // Premier affichage de la librairie du dossier ( entrainement, prediction )
                    displayDataset( $dataSetElement, datasetCategory );

                    if (successCallback) {
                        successCallback( _filteredDatasetsJson );
                    }
                });

            } else {

                // On a déjà téléchargé la liste des images du dossier ( entrainement, prediction )
                if (datasetFilterFunction) {
                    _filteredDatasetsJson = datasetFilterFunction( _datasetsJson[datasetCategory] );
                } else {
                    _filteredDatasetsJson = _datasetsJson[datasetCategory];
                }

                // Par défaut on ouvre la dernière catégorie affichée si elle existe dans le dossier ( entrainement, prediction )
                let datasetExists = getDatasetByName(datasetCategory, currentDatasetName);
                if (datasetExists) {
                    displayDataset( $dataSetElement, datasetCategory, currentDatasetName );
                } else {
                    displayDataset( $dataSetElement, datasetCategory );
                }

                if (successCallback) {
                    successCallback( _filteredDatasetsJson );
                }
            }
        }
    }

    function loadJsonDataset( datasetCategory, successCallback ) {
        let datasetsURL = datasetFolder + '/api/by_folder?folder=' + datasetCategory;
        $.ajax({
            url : datasetsURL,
            success : function(json) {
                if (successCallback) {
                    successCallback(json);
                }
            }
        });
    }

    // Dataset :
    function checkForPredictionDatasets( dataset1, dataset2, datasetFilterFunction, successCallback ) {

        $dataSetElement = $('.navigable-images-grid');
        if ($dataSetElement.length) {

            let datasetCategory = $dataSetElement.data('category');
            if ((_datasetsJson === undefined) || (_datasetsJson[datasetCategory] === undefined)) {

                // console.log("Appel à l'API des images de prédiction", datasetCategory, dataset1, dataset2);

                // On interroge l'API pour récupérer la liste des sous-dossiers et leurs contenus pour une catégorie ( entrainement, prediction )
                loadPredictionJsonDataset(datasetCategory, dataset1, dataset2, function (json) {

                    if (_datasetsJson === undefined) {
                        _datasetsJson = [];
                    }

                    // On stocke le retour de l'API pour un dossier ( entrainement, prediction)
                    _datasetsJson[datasetCategory] = json.datasets;

                    if (datasetFilterFunction) {
                        _filteredDatasetsJson = datasetFilterFunction(json.datasets);
                    } else {
                        _filteredDatasetsJson = json.datasets;
                    }

                    // Premier affichage de la librairie du dossier ( entrainement, prediction )
                    displayDataset($dataSetElement, datasetCategory);

                    if (successCallback) {
                        successCallback(_filteredDatasetsJson);
                    }
                });
            }
            else
            {
                // ON a déjà chargé ce dataSet, on affiche les images sans télécharger les données JSON

                if (datasetFilterFunction) {
                    _filteredDatasetsJson = datasetFilterFunction( _datasetsJson[datasetCategory] );
                } else {
                    _filteredDatasetsJson = _datasetsJson[datasetCategory];
                }

                displayDataset($dataSetElement, datasetCategory);
            }
        }
    }

    function loadPredictionJsonDataset( datasetCategory, dataset1, dataset2, successCallback ) {
        let datasetsURL = datasetFolder + '/api/by_categories?folder=' + datasetCategory + '&dataset1=' + dataset1 + '&dataset2=' + dataset2;
        $.ajax({
            url : datasetsURL,
            success : function(json) {
                if (successCallback) {
                    successCallback(json);
                }
            }
        });
    }

    function previousDataset() {
        let datasetsJsonForFolder = _filteredDatasetsJson;
        let i, n = datasetsJsonForFolder.length, dataset;
        for(i=0;i<n;i++) {
            dataset = datasetsJsonForFolder[i];
            if (dataset.folder === currentDatasetName)
            {
                let previousDatasetIndex = i > 0 ? i - 1 : n - 1;
                let previousDataset = datasetsJsonForFolder[previousDatasetIndex];

                displayDataset( $dataSetElement, currentDatasetCategory, previousDataset.folder );
                break;
            }
        }
    }

    function nextDataset() {
        let datasetsJsonForFolder = _filteredDatasetsJson;
        let i, n = datasetsJsonForFolder.length, dataset;
        for(i=0;i<n;i++) {
            dataset = datasetsJsonForFolder[i];
            if (dataset.folder === currentDatasetName)
            {
                let nextDatasetIndex = i < n - 1 ? i + 1 : 0;
                let nextDataset = datasetsJsonForFolder[nextDatasetIndex];

                displayDataset( $dataSetElement, currentDatasetCategory, nextDataset.folder );
                break;
            }
        }
    }

    function getDatasetByName( datasetCategory, datasetName ) {
        let datasetsJsonForFolder = _filteredDatasetsJson;
        let i, n = datasetsJsonForFolder.length, dataset;
        for(i=0;i<n;i++) {
            dataset = datasetsJsonForFolder[i];
            if (dataset.folder === datasetName) {
                return dataset;
            }
        }

        // Par défaut
        return datasetsJsonForFolder[0];
    }

    function displayDataset( $divDataSet, datasetCategory, datasetName ) {

        $divDataSet.empty();

        let dataset = getDatasetByName( datasetCategory, datasetName );
        if (dataset)
        {
            currentDatasetName = dataset.folder;
            currentDatasetCategory = datasetCategory;

            let templateHTML = template( { datasetpath: datasetFolder, datasetname: currentDatasetName, files : dataset.files } );
            $divDataSet.append( templateHTML );
        }
    }


    function updateDatasetSelector( $select, datasetsOfCategoryJson ) {

        let selectHtml = '';
        selectHtml += '<option selected="selected">'+i18n('Sélectionner une catégorie')+'</option>';

        let i, n = datasetsOfCategoryJson.length, categoryJson;
        for(i=0;i<n;i++) {
            categoryJson = datasetsOfCategoryJson[i];
            selectHtml += '<option value="'+ categoryJson.folder +'">' + i18n(categoryJson.folder) + '</option>';
        }

        $select.removeAttr("disabled");
        $select.html(selectHtml);


    }


    //
    // Sélection d'images pour l'entrainement
    //

    // Sauvegarde le HTML de la galerie d'images sélectionnées
    function saveImagesSelection( route, element ) {
        // console.log("saveImagesSelection", route, element);

        if (element === undefined) {
            element = $('.selected-images-grid');
        }

        _datasetSelections[route] = element.html();
    }

    function saveImagesCategory( route, category ) {
        _datasetCategories[route] = category;
    }

    function clearRouteData( route ) {

        if ( _datasetSelections ) {
            delete _datasetSelections[route];
        }

        if ( _datasetCategories ) {
            delete _datasetCategories[route];
        }
    }


    function getImagesCategory( route ) {
        return _datasetCategories[route];
    }

    function getStoredImageSelectionForRoute( route ) {
        return _datasetSelections[route];
    }

    function getInfosFromStoredImageSelectionForRoute( route ) {

        let imagesHtml = getStoredImageSelectionForRoute( route );
        let imagesNodes = $($.parseHTML(imagesHtml));
        let imagesPaths = [];
        let imageDatasetName;

        imagesNodes.find('img').each(function(no, item) {
            let itemElement = $(item);
            imagesPaths.push( itemElement.attr('data-path') );
            imageDatasetName = itemElement.attr('data-datasetname');
        });

        return { paths: imagesPaths, datasetName: imageDatasetName };
    }

    function resetOrRestoreImagesSelection( route, nbImages, $divElement ) {

        if ( $divElement === undefined ) {
            $divElement = $( '.selected-images-grid');
        }

        // On est déjà passé par cette étape, on affiche ce qu'on avait choisi
        let storedSelection = getStoredImageSelectionForRoute( route );
        if ( storedSelection !== undefined )
        {

            $divElement.html( storedSelection );
            return true;
        }

        // On a pas encore sélectionné ces images, on affiche les emplacements vides
        resetImagesSelection( nbImages, $divElement );

        return false;
    }

    function restoreImagesSelection( route, $divElement ) {

        let storedSelection = getStoredImageSelectionForRoute( route );
        if ( storedSelection !== undefined )
        {
            $divElement.html( storedSelection );
            return true;
        }

         return false;
    }

    function resetImagesSelection( nbImages, $divElement ) {

        if ( isNaN(nbImages)) {
            nbImages = IMAGES_SELECTION_LENGTH;
        }

        if ( $divElement === undefined ) {
            $divElement = $( '.selected-images-grid');
        }

        let i, ulHtml = '';

        ulHtml += '<ul>';

        for(i=0;i<nbImages;i++) {
            ulHtml += '<li></li>';
        }

        ulHtml += '</ul>';

        $divElement.html(ulHtml);
    }

    function isImagesSelectionComplete( $divElement ) {
        return getImagesSelectionCount( $divElement ) === IMAGES_SELECTION_LENGTH;
    }

    function isWebcamImagesComplete( $divElement ) {
        return getImagesSelectionCount( $divElement ) === IMAGES_WEBCAM_LENGTH;
    }

    function getImagesSelectionCount( $divElement ) {

        if ($divElement === undefined) {
            $divElement = $( '.selected-images-grid');
        }

        let nbImages = 0;

        $('li', $divElement).each(function(no, item) {
            let $li = $(item);
            if ( $li.children().length > 0 ) {
                nbImages++;
            }
        });

        return nbImages;
    }

    function addImageToSelection( $imageElement, checkIfFromSameDataset ) {

        if (checkIfFromSameDataset === undefined) {
            checkIfFromSameDataset = true;
        }

        // Dataset de l'image :
        let datasetImage = $imageElement.find('img').data('datasetname');

        if (checkIfFromSameDataset)
        {
            // Est ce qu'elle appartient au même dataset ?
            let $datasetNamesSelector = $('.dataset-names-selector');
            let previousVal = $datasetNamesSelector.val();
            $datasetNamesSelector.val(datasetImage);

            // Si ce n'est pas le cas, on efface les images déjà présentes
            if ( previousVal !== datasetImage ) {
                resetImagesSelection();
            }
        }

        let $divElement = $( '.selected-images-grid');
        let imageAdded = false;

        // On vérifie si cette image n'est pas déjà présente
        let imageURL = $('img', $imageElement ).attr('src');
        let alreadySelected = false;
        let selectedImageCount = 0;

        $('li', $divElement).each(function(no, item) {
            let $li = $(item);

            let $imageItem = $('img', $li);
            if ($imageItem.length) {
                selectedImageCount++;
            }

            if ($imageItem.attr("src") === imageURL ) {
                alreadySelected = true;

                // L'image était déjà sélectionnée, on la retire de la sélection
                $li.trigger("click");
                $imageElement.removeClass("selected");
            }
        });

        if ( ! alreadySelected && (selectedImageCount < 10) ) {

            // On ajoute une coche sur l'image de la bibliothèque
            $imageElement.addClass('selected');

            // On cherche le premier <li> sans enfant pour le remplacer par celui sur lequel on a cliqué
            $('li', $divElement).each(function (no, item) {

                if (!imageAdded) {
                    let $li = $(item);
                    if ($li.children().length === 0) {
                        $li.replaceWith( $imageElement.clone().removeClass("selected") );
                        imageAdded = true;
                    }
                }
            });
        }

        return imageAdded;
    }

    //Cas particulier des images de la webcam

    function increaseWebcamImagesSelectionIfComplete() {
        let $divElement = $( '.selected-images-grid');
        if( getImagesSelectionCount() === $('li', $divElement).length )
        {
            $divElement.find('ul').append('<li></li>');
        }
    }


    //
    // Entités ML5
    //

    function initPreTrainedClassifier() {

        // Création d'un classifier d'images
        initFeaturesExtractionClassifier(function () {

            closeProgressPopUp();

            // Message d'attente
            openProgressPopUp(i18n("Chargement des données de pré-entrainement..."));

            imageClassifier.load(datasetFolder + '/pretrained_tuto2.json').then(function () {
                closeProgressPopUp();
            });
        });
    }

    function isImagesClassifierExists() {
        return imageClassifier !== undefined;
    }

    function initFeaturesExtractionClassifier( successCallback ) {

        if( successCallback === undefined ) {
            successCallback = modelReady;
        }

        // Message d'attente
        openProgressPopUp(i18n("Chargement du modèle..."));

        if (KNN)
        {
            if (! featureExtractor || !imageClassifier )
            {
                // Modèle MobileNet + FeatureExtractor + KNN
                featureExtractor = ml5.featureExtractor("MobileNet", successCallback);
                imageClassifier = ml5.KNNClassifier();
            }
            else
            {
                // Si le KNN classifier existe déjà, on se contente de supprimer les résultats d'analyse des images
                imageClassifier.clearAllLabels();

                if(successCallback) successCallback();
            }
        }
        else
        {
            // Modèle MobileNet + FeatureExtractor
            featureExtractor = ml5.featureExtractor("MobileNet", successCallback);
            imageClassifier = featureExtractor.classification();
        }
    }

    function addImagesToImagesClassifier( $images, withTraining, successCallback ) {

        if (imageClassifier)
        {
            let imagesCount = $images.length;
            if (imagesCount > 0)
            {
                openProgressPopUp();

                displayProgressPopUp( i18n("Ajout des images") + "..." );

                let sequence = Promise.resolve();

                $images.each(function(no, image)
                {
                    sequence = sequence.then(function()
                    {
                        return new Promise(function(resolve, reject) {

                            // console.log("ADD IMAGE", no);

                            setTimeout(function ()
                            {
                                let $image = $(image);
                                let datasetName = $image.data('datasetname');
                                let classifierPromise;

                                    // En cas de succès :
                                let successFunction = function(result) {

                                    let pourcentage = Math.floor(100 * ((no + 1) / $images.length));
                                    displayProgressPopUp(i18n("Ajout des images") + " : " + pourcentage + "%");

                                    if (no === $images.length - 1) {
                                        closeProgressPopUp();
                                    }

                                    resolve();
                                };

                                // En cas d'erreur :
                                let errorFunction = function(err) {
                                    console.log(err);
                                 };


                                if (KNN)
                                {
                                    // K-Nearest neighbours
                                    const features = featureExtractor.infer( image );
                                    imageClassifier.addExample(features, datasetName);

                                    classifierPromise = Promise.resolve();
                                    classifierPromise.then( successFunction, errorFunction);

                                    // console.log(features.dataSync());
                                }
                                else
                                {
                                    // Feature extractor
                                    classifierPromise = imageClassifier.addImage( image, datasetName);
                                    classifierPromise.then( successFunction, errorFunction);
                                }

                            }, 100);
                        });
                    });
                });

                sequence.then(function()
                {
                    return new Promise(function(resolve, reject) {

                        // Fermeture du popUp
                        closeProgressPopUp();

                        if (withTraining) {
                            trainFeaturesExtractionClassifier( successCallback );
                        } else {
                            successCallback();
                        }

                        resolve();
                    });
                });

            }
            else
            {
                console.log("Pas d'images sélectionnées à ajouter au modèle");
            }
        }
        else
        {
            console.log("Pas de classifier");
        }
    }

    // RQ : cette opération n'existe pas pour la méthode KNN ( K-Nearest neighbours )
    function trainFeaturesExtractionClassifier( successCallback ) {

        if (imageClassifier)
        {
            openProgressPopUp();
            displayProgressPopUp( "Entrainement du modèle en cours..." );

            imageClassifier.train(function(lossValue) {

                // console.log(lossValue);

                if (lossValue === null) {

                    closeProgressPopUp();

                    if (successCallback) {
                        successCallback();
                    }
                }
            });
        }
    }

    function predictImageWithClassifier( image, errorSuccessCallback ) {

        if (KNN)
        {
            // K-nearest neightbours
            const features = featureExtractor.infer(image);
            imageClassifier.classify(features, errorSuccessCallback );
        }
        else
        {
            // Feature extractor
            imageClassifier.classify(image, errorSuccessCallback);
        }
    }

    function modelReady() {
        closeProgressPopUp();
    }


    //
    // Popup
    //

    function openProgressPopUp( message ) {

        let popup = $('.popup');
        popup.removeClass('hidden');
        popup.append('<div class="popup-content"></div>');

        if (message) {
            displayProgressPopUp(message);
        }
    }

    function closeProgressPopUp() {
        let popup = $('.popup');
        popup.addClass('hidden');
        popup.empty();

    }

    function displayProgressPopUp( message ) {
        let popupContent = $('.popup-content');
        popupContent.empty();
        popupContent.append( '<p>' + message + '</p>' );
    }

    //
    // Webcam
    //

    function initWebcam( successCallback ) {

        let video;

        // Create video element that will contain the webcam image
        video = document.createElement('video');
        video.setAttribute('autoplay', '');
        video.setAttribute('playsinline', '');

        // Setup webcam
        navigator.mediaDevices.getUserMedia({ video: true, audio: false })
            .then((stream) => {
                video.srcObject = stream;
                video.width = 250;
                video.height = 184;

                video.addEventListener('playing', webcamIsPlaying);
                video.addEventListener('paused' , webcamIsNotPlaying);

                if (successCallback) {
                    successCallback();
                }
            });

        return video;
    }

    function closeWebcam() {
        if (webcamVideo) {
            webcamVideo.removeEventListener('playing', webcamIsPlaying);
            webcamVideo.removeEventListener('paused', webcamIsNotPlaying);
            webcamVideo.pause();

            let webcamStream = webcamVideo.srcObject;
            if (webcamStream) {
                webcamStream.getTracks()[0].stop();
            }

            webcamVideo.src = "";
        }

        webcamVideo = null;
    }

    function webcamIsPlaying() {
        videoPlaying = true;
    }

    function webcamIsNotPlaying() {
        videoPlaying = false;
    }

    function getVideoSnapShotWrappedAsLiWithCheckbox() {

        var $li  = getVideoSnapShotWrappedAsLi();
        $li.addClass('with-checkbox');
        $li.append('<label class="styled-checkbox"><input type="checkbox" checked="checked" /><span class="checkmark"></span></label>');

        return $li;
    }


    function getVideoSnapShotWrappedAsLi() {

        let img = getVideoSnapShot();

        let li = document.createElement('li');
        li.appendChild(img);

        let $li = $(li);
        $li.addClass('with-image-in-span');
        $li.append('<span class="image">');

        let $span = $li.find('span');
        $span.addClass( 'dataset-image' );
        $span.css('background-image', 'url(' +img.src+ ')' );

        let $img  = $li.find('img');
        $img.addClass("hidden-dataset-image");

        return $li;
    }

    function getVideoSnapShot() {

        let videoElement = $('video')[0];
        let canvas = document.createElement('canvas');
        canvas.height = videoElement.videoHeight;
        canvas.width = videoElement.videoWidth;

        let ctx = canvas.getContext('2d');
        ctx.drawImage(videoElement, 0, 0, canvas.width, canvas.height);

        let img = new Image();
        img.src = canvas.toDataURL();

        return img;
    }

    function playVideo(videoElement) {

        if (videoElement) {

            let videoParent = videoElement.closest('.video-parent');

            let video = videoElement[0];
            video.addEventListener('ended', endOfVideo, false);

            var textTracks = video.textTracks;
            if (textTracks && (textTracks.length > 0)) {
                var textTrack = textTracks[0];
                textTrack.mode = "showing";
            }

            if (video.paused === true) {
                video.play();
                videoParent.addClass('is-playing');
            } else {
                video.pause();
                videoParent.removeClass('is-playing');
            }
        }

    }

    function endOfVideo(e) {

        let video = e.target;
        if (video) {
            video.removeEventListener('ended', endOfVideo, false);
        }

        if ($('.navigation-route.next-link a').length) {
            $('.navigation-route.next-link a').trigger('click');
        } else if ($('.last-video-before-credits')) {
            $('.credits-link a').trigger('click');
        }
    }

    //
    //
    //

    function startModifyingCategory1(category1) {

        // On doit :

        // - rendre le champ du nom de la catégorie 1 modifiable
        var $titreCategorie1 = $('.instructions-panel-header-category1 h4');
        var inputCategorie1 = $('.instructions-panel-header-category1 input[type=text]');
        $titreCategorie1.addClass('hidden');
        inputCategorie1.removeClass('hidden').val( $titreCategorie1.html() );

        // - rendre la galerie 1 modifiable ( suppression )
        $('.category1-images-grid').addClass('selected-images-grid');

        // - changer le bouton "modifier" en "valider"
        $('.modify-category1-button').addClass('hidden');
        $('.validate-category1-button').removeClass('hidden').addClass('active');

        // - rendre la catégorie 2 inactive
        $('.instructions-panel-header-category2').addClass('inactive');
        $('.category2-images-grid').addClass('inactive');
        $('.modify-category2-button').addClass('inactive');

        // - rendre le bouton Entraîner inactif
        $('.train-selections-button').addClass('inactive');

    }

    function startModifyingCategory2(category2) {

        // On doit :

        // - rendre le champ du nom de la catégorie 2 modifiable
        var $titreCategorie2 = $('.instructions-panel-header-category2 h4');
        var inputCategorie2 = $('.instructions-panel-header-category2 input[type=text]');
        $titreCategorie2.addClass('hidden');
        inputCategorie2.removeClass('hidden').val( $titreCategorie2.html() );

        // - rendre la galerie 2 modifiable ( suppression )
        $('.category2-images-grid').addClass('selected-images-grid');

        // - changer le bouton "modifier" en "valider"
        $('.modify-category2-button').addClass('hidden');
        $('.validate-category2-button').removeClass('hidden').addClass('active');

        // - rendre la catégorie 1 inactive
        $('.instructions-panel-header-category1').addClass('inactive');
        $('.category1-images-grid').addClass('inactive');
        $('.modify-category1-button').addClass('inactive');

        // - rendre le bouton Entraîner inactif
        $('.train-selections-button').addClass('inactive');

    }

    function stopModifyingCategory1( route, firstValidation ) {

        // On doit :

        // - rendre le champ du nom de la catégorie 1 non modifiable
        var $inputCategorie1 = $('.instructions-panel-header-category1 input[type=text]');
        var categorie1 = $inputCategorie1.val().toLowerCase();

        var $titreCategorie1 = $('.instructions-panel-header-category1 h4');
        $titreCategorie1.removeClass('hidden').html( categorie1 );
        $inputCategorie1.addClass('hidden');

        var category1_ImagesGrid = $('.category1-images-grid');
        var category2_ImagesGrid = $('.category2-images-grid');

        // - rendre la galerie 1 non modifiable ( on inactive la suppression )
        category1_ImagesGrid.removeClass('selected-images-grid');
        $('img', category1_ImagesGrid).attr('data-datasetname', categorie1);

        // - changer le bouton "valider" en "modifier"
        $('.modify-category1-button').removeClass('hidden');
        $('.validate-category1-button').removeClass('inactive').removeClass('active').addClass('hidden');

        // - sauvegarder les changements
        saveImagesSelection( route, category1_ImagesGrid);
        saveImagesCategory( route, categorie1 );

        // - rendre la catégorie 2 active
        $('.instructions-panel-header-category2').removeClass('inactive');
        category2_ImagesGrid.removeClass('inactive');
        $('.modify-category2-button').removeClass('inactive');

        console.log("firstValidation", firstValidation);

        if (firstValidation === true)
        {
            // Première validation de la catégorie 1

            // - Afficher la galerie 1 comme non active
            $('.instructions-panel-header-category1').addClass('inactive');
            category1_ImagesGrid.addClass('inactive');
            $('.modify-category1-button').addClass('inactive');

            // - rendre le champ du nom de la catégorie 2 modifiable
            var $titreCategorie2 = $('.instructions-panel-header-category2 h4');
            var inputCategorie2 = $('.instructions-panel-header-category2 input[type=text]');
            $titreCategorie2.addClass('hidden');
            inputCategorie2.removeClass('hidden').val( $titreCategorie2.html() );

            // - la prise de vue (ou l'import) d'une image s'ajoutera à la sélection 2
            category2_ImagesGrid.addClass('selected-images-grid');
            $('.validate-category2-button').addClass('active');
        }
        else
        {
            // Cas où on est dans le cycle "Modifier/Valider" :

            // - rendre le bouton Entraîner actif
            $('.train-selections-button').removeClass('inactive');


            $('.train-selections-button').removeClass('inactive');
            if ( ! isImagesSelectionComplete(category1_ImagesGrid) || ! isImagesSelectionComplete(category2_ImagesGrid) ) {
                $('.train-selections-button').addClass('inactive');
            }
        }

    }

    function stopModifyingCategory2( route ) {

        // On doit :

        // - rendre le champ du nom de la catégorie 2 non modifiable
        var $inputCategorie2 = $('.instructions-panel-header-category2 input[type=text]');
        var categorie2 = $inputCategorie2.val().toLowerCase();

        var $titreCategorie2 = $('.instructions-panel-header-category2 h4');
        $titreCategorie2.removeClass('hidden').html( categorie2 );
        $inputCategorie2.addClass('hidden');

        // - rendre la galerie 2 non modifiable ( suppression )
        $('.category2-images-grid').removeClass('selected-images-grid');
        $('.category2-images-grid img').attr('data-datasetname', categorie2);

        // - changer le bouton "valider" en "modifier"
        $('.modify-category2-button').removeClass('hidden');
        $('.validate-category2-button').removeClass('inactive').removeClass('active').addClass('hidden');

        // - rendre la catégorie 1 active
        $('.instructions-panel-header-category1').removeClass('inactive');
        $('.category1-images-grid').removeClass('inactive');
        $('.modify-category1-button').removeClass('inactive');

        // - rendre le bouton Entraîner actif
        $('.train-selections-button').removeClass('inactive');

        // - sauvegarder les changements
        saveImagesSelection( route, $('.category2-images-grid'));
        saveImagesCategory( route, categorie2 );


        $('.train-selections-button').removeClass('inactive');
        if ( ! isImagesSelectionComplete($('.category1-images-grid')) || ! isImagesSelectionComplete($('.category2-images-grid')) ) {
            $('.train-selections-button').addClass('inactive');
        }
    }


    })( jQuery );
