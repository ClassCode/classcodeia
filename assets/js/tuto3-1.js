(function($){

    // http://127.0.0.1:8080/edsa-websites/classcodeia/app/tuto3/experimenter/etape2
    // http://127.0.0.1:8080/edsa-websites/inria-tutos/app/test_images/

    // https://projects.invisionapp.com/share/ZMULMNT879R#/screens/394634306
    // https://docs.google.com/presentation/d/1yz2ib1BCuWb9aY3pAX0cWOII6e2S1UtkCkWzAg7xdpk/edit#slide=id.g70a7ca2b8c_0_1296

    // http://talkerscode.com/webtricks/preview-image-before-upload-using-javascript.php


    // Support pour le navigateur Microsoft Edge, version non-Chromium ( Microsoft Edge 44.18362.449.0 ):
    // 1. Un polyfill a été également ajouté pour le support de TextEncoding : https://github.com/inexorabletash/text-encoding
    // 2. Desactivation du passage par la carte graphique via WebGL
    if (document.documentMode || /Edge/.test(navigator.userAgent)) {
        _tfengine.ENV.set('WEBGL_PACK', false);
    }

    // Elements
    let $application = $('.tuto-ia-application');
    let $header  = $('#step-header');
    let $content = $('#step-contents');
    let $footer  = $('#step-footer');

    // BaseURL : écrit dans le index.php via PHP
    let baseURL = $application.attr('data-baseURL');

    // Lang : ecrit dans le index.php via PHP
    let lang = $application.attr('data-lang');
    // Lang file extension used for subtitles
    let langExt = lang && lang === 'fr' ? '':`-${lang}`;
    let videoExt = langExt;

    // Chemins
    let dataFolder = baseURL + "/data";
    let assetsFolder = baseURL + "/assets";

    // Tutoriel 1 :
    let appBaseRoute = baseURL + "/app/tuto3-1";
    let tutorialFolder = dataFolder + "/tuto3-1";

    // Templates HandleBars :
    Handlebars.templateFolder = tutorialFolder + '/templates/';
    Handlebars.templates = [];

    // Mise en cache des templates JSON des différents écrans pour ne pas les charger plusieurs fois
    let _tutorialJson;

    let currentAbsoluteRoute;
    let currentRelativeRoute;
    let memoRelativeRoute="";

    // QCM :
    let qcm_tabImages=[];
    let qcm_index;
    let qcm_nextTimer=1500;
    let storeAIImages="";
    let qcm_steps=[-1,-1,-1,-1,-1];

    let storeAISteps="";


    // -------------------------------------------------------------------------------------------------------- //
    // FONCTIONS LIEES AU TUTORIEL
    // -------------------------------------------------------------------------------------------------------- //

    //
    // Evènements
    //

    // RQ : Les évènements souris sont détectés à la racine du DOM de l'application

    // Avant le chargement de la template :
    function beforeLoadingTemplateJsonForRoute( route ) {

        let templateParams = {};

        route = route.split(appBaseRoute).join('');
        console.log("....................."+route);
        switch(route) {

            case '/':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto3/mp4/tuto3-activite1-vid1" + videoExt + ".mp4";
                templateParams.posterPath = tutorialFolder + "/medias/poster-neutre.jpg";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto3-activite1-vid1" + langExt + ".vtt";
                break;
            case '/tester':
            case '/tester/etape1':
                qcm_tabImages=[];
                for(var i=1;i<12;i++){
                    qcm_tabImages[qcm_tabImages.length]="GAN1/Person-Gan-"+i+".jpg";
                }
                for(var i=1;i<11;i++){
                    qcm_tabImages[qcm_tabImages.length]="GAN2/true"+i+".jpg";
                }
                //-- vérification qu'il y a bien au moins 1 vrai et un faux
                let okshuffle=0;
                while (okshuffle==0){
                    qcm_tabImages=shuffleAffay(qcm_tabImages);
                    nbGan1=0;
                    nbGan2=0;
                    for(i=0;i<4;i++){
                       if(qcm_tabImages[i].substr(0,4)=="GAN1") {
                           nbGan1+=1;
                       }else{
                           nbGan2+=1;
                       }
                    }
                    if((nbGan1>0)&&(nbGan2>0)) {
                        okshuffle=1;
                    }else{
                        console.log("!!!!!!!!!!! reshuffle !!!!!!!!!!!!!!");
                    }
                }

                //console.log(qcm_tabImages);
                //console.log(qcm_tabImages.toString());
                storeData(qcm_tabImages.toString());
                qcm_index=0;
                qcm_steps=[-1,-1,-1,-1,-1];
                storeSteps(qcm_steps.toString());

                currentChapter='tester';

                break;

            case '/tester/etape2':
                qcm_index=1;
                storeAIImages=restoreAIImages();
                qcm_tabImages=storeAIImages.split(',');
                currentChapter='tester';
                break;
            case '/tester/etape3':
                qcm_index=2;
                storeAIImages=restoreAIImages();
                qcm_tabImages=storeAIImages.split(',');
                currentChapter='tester';
                break;
            case '/tester/etape4':
                qcm_index=3;
                storeAIImages=restoreAIImages();
                qcm_tabImages=storeAIImages.split(',');
                currentChapter='tester';
                break;
            case '/tester/etape5':
                qcm_index=4;
                storeAIImages=restoreAIImages();
                qcm_tabImages=storeAIImages.split(',');
                currentChapter='tester';
                break;

            case '/verifier':
            case '/verifier/etape1':
                storeAIImages=restoreAIImages();
                qcm_tabImages=storeAIImages.split(',');

                if(qcm_tabImages.length==0) {
                    for(var i=1;i<12;i++){
                        qcm_tabImages[qcm_tabImages.length]="GAN1/Person-Gan-"+i+".jpg";
                    }
                    for(var i=1;i<11;i++){
                        qcm_tabImages[qcm_tabImages.length]="GAN2/true"+i+".jpg";
                    }
                    qcm_tabImages=shuffleAffay(qcm_tabImages);
                }else{
                }

                qcm_index=0;
                qcm_steps=[-1,-1,-1,-1,-1];
                storeSteps(qcm_steps.toString());
                currentChapter='verifier';

                break;


            case '/verifier/etape2':
                qcm_index=1;
                storeAIImages=restoreAIImages();
                qcm_tabImages=storeAIImages.split(',');
                currentChapter='verifier';
                break;
            case '/verifier/etape3':
                qcm_index=2;
                storeAIImages=restoreAIImages();
                qcm_tabImages=storeAIImages.split(',');
                currentChapter='verifier';
                break;
            case '/verifier/etape4':
                qcm_index=3;
                storeAIImages=restoreAIImages();
                qcm_tabImages=storeAIImages.split(',');
                currentChapter='verifier';
                break;
            case '/verifier/etape5':
                qcm_index=4;
                storeAIImages=restoreAIImages();
                qcm_tabImages=storeAIImages.split(',');
                currentChapter='verifier';
                break;

            case '/comprendre/etape1':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto3/mp4/tuto3-activite1-vid2" + videoExt + ".mp4";
                templateParams.posterPath = tutorialFolder + "/medias/poster-neutre.jpg";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto3-activite1-vid2" + langExt + ".vtt";

                break;
            case '/conclure/etape1':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto3/mp4/tuto3-activite1-vid3" + videoExt + ".mp4";
                templateParams.posterPath = tutorialFolder + "/medias/poster-neutre.jpg";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto3-activite1-vid3" + langExt + ".vtt";

                break;

        }

        //-- dans tous les cas
        $('qcm-button').css('background-color','#5937B9');

        return templateParams;
    }

    // Une fois la template chargée et insérée dans le DOM :
    function afterInsertingTemplateForRoute( route ) {

        route = route.split(appBaseRoute).join('');

        console.log('Templated Loaded', route);

        console.log(memoRelativeRoute)

        let dataCompleteForCurrentStep;
        let infosCategory1, infosCategory2;

        switch(route) {
            case '/':
            case '/comprendre/etape1':
            case '/conclure/etape1':

                switch(memoRelativeRoute){
                    case "/comprendre":
                    case "/conclure":
                    case "/":
                        $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../tuto3/" href="../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../tuto3-1/" href="/">> IA ou Humains ?</a>')
                        break;
                    default:
                        $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../../tuto3/" href="../../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../../tuto3-1/" href="/">> IA ou Humains ?</a>')

                        break;

                }


                break;
            case '/tester/etape1':
            case '/verifier/etape1':

                qcm_proposal();

                //-- menu -----------------------
                if(memoRelativeRoute=="/tester") {
                    // console.log($(".title-link.navigation-route").html());
                    $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../tuto3/" href="../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../tuto3-1/" href="/">> IA ou Humains ?</a>')
                }else{
                    if(memoRelativeRoute=="/verifier") {
                        // console.log($(".title-link.navigation-route").html());
                        $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../tuto3/" href="../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../tuto3-1/" href="/">> IA ou Humains ?</a>')
                    }else{
                        // console.log($(".title-link.navigation-route").html());
                        $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../../tuto3/" href="../../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../../tuto3-1/" href="/">> IA ou Humains ?</a>')
                    }
                }

                break;

            case '/tester/etape2':
            case '/tester/etape3':
            case '/tester/etape4':
            case '/tester/etape5':
            case '/verifier/etape2':
            case '/verifier/etape3':
            case '/verifier/etape4':
            case '/verifier/etape5':

                qcm_proposal();

                //-- menu -----------------------
                // console.log($(".title-link.navigation-route").html());
                $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../../tuto3/" href="../../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../../tuto3-1/" href="/">> IA ou Humains ?</a>')

                break;
            case '/conclure/etape2':

                let contentHtml=$(".step-footer").html();
                contentHtml+='<ul class="navigation-next"><li class="navigation-route next-link next-chapter" ><a class="navigation-link forced" data-route="../../tuto3-2/" >Chapitre suivant</a></li></ul>';
                $(".step-footer").html(contentHtml);

                //-- menu -----------------------
                // console.log($(".title-link.navigation-route").html());
                $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../../tuto3/" href="../../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../../tuto3-1/" href="/">> IA ou Humains ?</a>')

                break;

            default:
                //-- menu -----------------------
                // console.log($(".title-link.navigation-route").html());
                $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../../tuto3/" href="../../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../../tuto3-1/" href="/">> IA ou Humains ?</a>')

                break;

        }
        //-- raz for all --------------
        $(".zoom-div").css('display','none');

    }

    // Si l'utilisateur clique sur un élement d'interface
    $application.on('click', function(e) {

        let target = $(e.target);
        let message, $li;

        // Liens basiques (credits)
        if (target.hasClass("basic-link"))
        {
            return;
        }

        e.preventDefault();

        if (target.hasClass("forced"))
        {

            let navigationRoute = target.data('route');

            document.location=navigationRoute;
        }
        else if (target.hasClass("navigation-link"))
        {
            let navigationLi = target.closest('.navigation-route');
            let navigationRoute = navigationLi.data('route');

            $.router.set({
                route: appBaseRoute + navigationRoute
            });
        }
        else if (target.hasClass("video-parent"))
        {
            let videoElement = target.find('video');
            playVideo(videoElement);
        }
        else if ( target.hasClass("video-background") || target.hasClass("video-infos")) {

            let videoParent = target.closest('.video-parent');
            let videoElement = videoParent.find('video');
            playVideo(videoElement);

        }
        else if (target.hasClass("outer-link"))
        {
            window.open(target.attr("href"));
        }
        else if (target.hasClass("title-link"))
        {
            document.location=baseURL+"/app/tuto3/";
        }
        else if (target.hasClass("qcm-button"))
        {
            // tester si bonne réponse
            let trueOrAi=qcm_tabImages[qcm_index].substr(0,4);

            if(currentChapter=='tester') {
                trueOrAi=qcm_tabImages[qcm_index].substr(0,4);
            }else{
                trueOrAi=qcm_tabImages[qcm_index+5].substr(0,4);
            }
             let goodResponse=false;
            if (target.hasClass("human")) {
                if(trueOrAi=="GAN2") {
                    goodResponse=true;
                }else{
                }
            }else{
                if(trueOrAi=="GAN1") {
                    goodResponse=true;
                }else{
                }
            }
            let audioPlayer = document.getElementById('audioPlayer1');
            let audioFile;
            if(goodResponse==true) {
                target.css('background-color','#00FF00');
                audioFile=tutorialFolder + '/medias/sounds/good.mp3';
                qcm_steps[qcm_index]=1;
            }else{
                target.css('background-color','#FF0000');
                audioFile=tutorialFolder + '/medias/sounds/bad.mp3';
                qcm_steps[qcm_index]=0;
            }

            storeSteps(qcm_steps.toString());
            showStepResponses();
            //console.log(audioFile);
            audioPlayer.src = audioFile;
            audioPlayer.play();

            var max_index=4;

            if(qcm_index<max_index) {
                qcm_index+=1;
                setTimeout(go_qcm,qcm_nextTimer);
            }else{
                if(currentChapter=='tester') {
                    setTimeout(goRoute,qcm_nextTimer,'/comprendre/etape1');
                }else{
                    setTimeout(goRoute,qcm_nextTimer,'/conclure/etape1');
                }

            }


        }
        else if (target.hasClass("qcm-zoom"))
        {
            $(".zoom-div").css('display','block');
        }
        else if (target.hasClass("zoom-div"))
        {
            $(".zoom-div").css('display','none');
        }
        else if (target.hasClass("credits-click"))
        {
            console.log("credits");
            goRoute('/');
        }
        
    });




    // -------------------------------------------------------------------------------------------------------- //
    // FONCTIONS GENERALES
    // -------------------------------------------------------------------------------------------------------- //

    //
    // Router
    // https://github.com/scssyworks/silkrouter/tree/feature/ver2
    //

    $.route(function (data) {

        currentAbsoluteRoute = data.route;

        let relativeRoute = currentAbsoluteRoute.split(appBaseRoute).join('');

        console.log("Route absolue", currentAbsoluteRoute, "Route relative", relativeRoute);
        memoRelativeRoute=relativeRoute;

        if (_tutorialJson === undefined) {
            loadChapitresJson( function( json ) {
                currentRelativeRoute = updateContentWithRoute( _tutorialJson = json, relativeRoute);
                console.log("currentRelativeRoute", currentRelativeRoute);
            });
        } else {
            currentRelativeRoute = updateContentWithRoute( _tutorialJson, relativeRoute);
        }
    });



    // init translations then init route
    initTutorielTranslations(baseURL, lang, tutorialFolder, function() {
        $.router.init();
    });



    // Retour forcé à l'accueil : par exemple, si l'utilisateur recharge une étape intermédiaire
    function backHome() {
        goRoute('/');
    }

    function goRoute( route ) {
        $.router.set({
            route: appBaseRoute + route
        });
    }


    //
    // Chargement du JSON des templates
    //

    function loadChapitresJson( successCallback ) {
        getTutorielJSON( tutorialFolder + '/json/chapitres.json', function( json ) {
            if(successCallback) {
                successCallback( json );
            }
        });
    }


    //
    // Affichage de la template de la route
    //

    function updateContentWithRoute( tutoJson, route ) {
        if(route=="/comprendre"){
            route="/comprendre/etape1";
        }else{
        }
        
        console.log("updateContentWithRoute", tutoJson, route);

        // La valeur de la route passée en paramètre peut être modifié ( ex : chapitre )
        let contentDescription = getStepDescriptionForRoute(tutoJson, route);
        let relativeRoute = contentDescription.step.route;

        console.log("relativeRoute", contentDescription, relativeRoute);

        // Préparation des données à injecter dans la template
        let templateParams = beforeLoadingTemplateJsonForRoute( relativeRoute );

        // Chargement de la template
        displayRoute( tutoJson, appBaseRoute, relativeRoute, $header, $content, $footer, contentDescription, templateParams );

        // Application des régles liés à la template après chargement
        afterInsertingTemplateForRoute(relativeRoute);

        return relativeRoute;
    }


    //
    // QCM
    //
    function qcm_proposal(){
        //console.log(tutorialFolder + '/medias/'+qcm_tabImages[qcm_index]);
        //console.log(qcm_tabImages);
        //console.log(qcm_index);
        if(currentChapter=='tester') {
            $(".image-for-qcm").css('background-image', 'url(' +tutorialFolder + '/medias/'+qcm_tabImages[qcm_index]+ ')' );
            $(".zoom-div").css('background-image', 'url(' +tutorialFolder + '/medias/'+qcm_tabImages[qcm_index]+ ')' );
        }else{
            $(".image-for-qcm").css('background-image', 'url(' +tutorialFolder + '/medias/'+qcm_tabImages[qcm_index+5]+ ')' );
            $(".zoom-div").css('background-image', 'url(' +tutorialFolder + '/medias/'+qcm_tabImages[qcm_index+5]+ ')' );
        }

        storeAISteps=restoreSteps();
        qcm_steps=storeAISteps.split(',');
        if(qcm_steps.length!=5) {
            qcm_steps=[-1,-1,-1,-1,-1];
        }else{
        }
        showStepResponses();


    }
    function showStepResponses(){
        for(i=0;i<5;i++){
            let valStep=parseInt(qcm_steps[i]);
            switch(valStep){
                case -1:
                    break;
                case 0:
                    $(".step-footer ul.navigation li:nth-child("+(i+1)+")").css('background-color','#FF0000');
                    break;
                case 1:
                    $(".step-footer ul.navigation li:nth-child("+(i+1)+")").css('background-color','#00FF00');
                    break;
            }
        }
    }
    function go_qcm(){

        goRoute('/'+currentChapter+'/etape'+(qcm_index+1));
    }


    function playVideo(videoElement) {

        if (videoElement) {

            let videoParent = videoElement.closest('.video-parent');

            let video = videoElement[0];
            video.addEventListener('ended', endOfVideo, false);

            var textTracks = video.textTracks;
            if (textTracks && (textTracks.length > 0)) {
                var textTrack = textTracks[0];
                textTrack.mode = "showing";
            }

            if (video.paused === true) {
                video.play();
                videoParent.addClass('is-playing');
            } else {
                video.pause();
                videoParent.removeClass('is-playing');
            }
        }
    }

    function endOfVideo(e) {

        let video = e.target;
        if (video) {
            video.removeEventListener('ended', endOfVideo, false);
        }
        /*
        if ($('.navigation-route.next-link a').length) {
            $('.navigation-route.next-link a').trigger('click');
        } else if ($('.last-video-before-credits')) {
            $('.credits-link a').trigger('click');
        }
        */
    }


    // ========================================================================
//  STORE data
// ========================================================================


    function storeData( theArray) {
        if (typeof(Storage) !== "undefined") {
            // Code for localStorage/sessionStorage.
            localStorage.setItem("storeAIImages", theArray);
        } else {
            // Sorry! No Web Storage support..
        }
    }
    function storeSteps( theArray) {
        if (typeof(Storage) !== "undefined") {
            // Code for localStorage/sessionStorage.
            localStorage.setItem("storeSteps", theArray);
        } else {
            // Sorry! No Web Storage support..
        }
    }

    function restoreAIImages() {
        theData="";
        if (typeof(Storage) !== "undefined") {
            // Code for localStorage/sessionStorage.
            theData=localStorage.getItem("storeAIImages");
            if(typeof theData != 'string') {
                theData="";
            }else{

            }
        } else {
            // Sorry! No Web Storage support..
            theData="notsupported";
        }
        return theData;
    }
    function restoreSteps() {
        theData="";
        if (typeof(Storage) !== "undefined") {
            // Code for localStorage/sessionStorage.
            theData=localStorage.getItem("storeSteps");
            if(typeof theData != 'string') {
                theData="";
            }else{

            }
        } else {
            // Sorry! No Web Storage support..
            theData="notsupported";
        }
        return theData;
    }



})( jQuery );
