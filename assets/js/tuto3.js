(function($){

    // http://127.0.0.1:8080/edsa-websites/classcodeia/app/tuto3/experimenter/etape2
    // http://127.0.0.1:8080/edsa-websites/inria-tutos/app/test_images/

    // https://projects.invisionapp.com/share/ZMULMNT879R#/screens/394634306
    // https://docs.google.com/presentation/d/1yz2ib1BCuWb9aY3pAX0cWOII6e2S1UtkCkWzAg7xdpk/edit#slide=id.g70a7ca2b8c_0_1296

    // http://talkerscode.com/webtricks/preview-image-before-upload-using-javascript.php


    // Support pour le navigateur Microsoft Edge, version non-Chromium ( Microsoft Edge 44.18362.449.0 ):
    // 1. Un polyfill a été également ajouté pour le support de TextEncoding : https://github.com/inexorabletash/text-encoding
    // 2. Desactivation du passage par la carte graphique via WebGL
    if (document.documentMode || /Edge/.test(navigator.userAgent)) {
        _tfengine.ENV.set('WEBGL_PACK', false);
    }

    // Elements
    let $application = $('.tuto-ia-application');
    let $header  = $('#step-header');
    let $content = $('#step-contents');
    let $footer  = $('#step-footer');

    // BaseURL : écrit dans le index.php via PHP
    let baseURL = $application.attr('data-baseURL');

    // Lang : ecrit dans le index.php via PHP
    let lang = $application.attr('data-lang');
    // Lang file extension used for subtitles
    let langExt = lang && lang === 'fr' ? '':`-${lang}`;
    let videoExt = langExt;

    // Chemins
    let dataFolder = baseURL + "/data";
    let assetsFolder = baseURL + "/assets";

    // Tutoriel 1 :
    let appBaseRoute = baseURL + "/app/tuto3";
    let tutorialFolder = dataFolder + "/tuto3";

    // Templates HandleBars :
    Handlebars.templateFolder = tutorialFolder + '/templates/';
    Handlebars.templates = [];

    // Mise en cache des templates JSON des différents écrans pour ne pas les charger plusieurs fois
    let _tutorialJson;

    let currentAbsoluteRoute;
    let currentRelativeRoute;


    const IMAGES_WEBCAM_LENGTH = 20;

    // Nombre d'images dans la librairie de test
    const IMAGES_PREDICTION_LENGTH = 40;

    let imageClassifier;

    // Webcam :
    let webcamVideo;



    // -------------------------------------------------------------------------------------------------------- //
    // FONCTIONS LIEES AU TUTORIEL
    // -------------------------------------------------------------------------------------------------------- //

    //
    // Evènements
    //
    if(pageTuto3=="conclusion") {
        goRoute("/conclusion");
    }else{
    }
    // RQ : Les évènements souris sont détectés à la racine du DOM de l'application

    // Avant le chargement de la template :
    function beforeLoadingTemplateJsonForRoute( route ) {

        let templateParams = {};

        route = route.split(appBaseRoute).join('');
        switch(route) {

            case '/':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto3/mp4/tuto3-0-preambule" + videoExt + ".mp4";
                templateParams.posterPath = tutorialFolder + "/medias/poster-journal.jpg";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto3-0-preambule" + langExt + ".vtt";
                break;

            case '/conclusion':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto3/mp4/tuto3-conclusion" + videoExt + ".mp4 ";
                templateParams.posterPath = tutorialFolder + "/medias/poster-neutre.jpg";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto3-conclusion" + langExt + ".vtt";

                break;

        }

        return templateParams;
    }

    // Une fois la template chargée et insérée dans le DOM :
    function afterInsertingTemplateForRoute( route ) {

        route = route.split(appBaseRoute).join('');

        console.log('Templated Loaded', route);

        let dataCompleteForCurrentStep;
        let infosCategory1, infosCategory2;

        switch(route) {

            case '/':
                let footerHtml=$footer.html();
                footerHtml='<ul class="navigation step-navigation"></ul><ul class="navigation-next">';
                footerHtml+='<li class=" next-link" ><a class="main-menu3" href="../../app/tuto3-1">IA OU HUMAINS ?</a></li>\n' +
                    '<li class=" next-link"  ><a class="main-menu3"  href="../../app/tuto3-2">You only look once</a></li>\n' +
                    '<li class=" next-link" ><a class="main-menu3"  href="../../app/tuto3-3">LE CRI DES ANIMAUX</a></li>\n' +
                    '<li class=" next-link" ><a class="main-menu3"  href="../../app/tuto3-4">ROBERT DE BARRETIN</a></li>';
                footerHtml+='</ul>';
                //footerHtml+='<span data-route="/credits" class="credits-link navigation-route"><a class="navigation-link" href="/credits">Crédits • CC • BY • SA  2020</a></span>';

                //console.log("fooooooter-----------");
                //console.log (footerHtml);
                $footer.html(footerHtml);

               break;

        }
    }

    // Si l'utilisateur clique sur un élement d'interface
    $application.on('click', function(e) {

        let target = $(e.target);
        console.log(target);
        let message, $li;

        // Liens basiques (credits)
        if (target.hasClass("basic-link"))
        {
            return;
        }

        e.preventDefault();

        if (target.hasClass("navigation-link"))
        {
            let navigationLi = target.closest('.navigation-route');
            let navigationRoute = navigationLi.data('route');

            $.router.set({
                route: appBaseRoute + navigationRoute
            });
        }
        else if (target.hasClass("video-parent"))
        {
            let videoElement = target.find('video');
            playVideo(videoElement);
        }
        else if ( target.hasClass("video-background") || target.hasClass("video-infos")) {

            let videoParent = target.closest('.video-parent');
            let videoElement = videoParent.find('video');
            playVideo(videoElement);

        }
        else if (target.hasClass("main-menu3"))
        {
            document.location=target.attr("href");
        }
        else if (target.hasClass("credits-click"))
        {
            console.log("credits");
            goRoute('/');
        }

    });




    // -------------------------------------------------------------------------------------------------------- //
    // FONCTIONS GENERALES
    // -------------------------------------------------------------------------------------------------------- //

    //
    // Router
    // https://github.com/scssyworks/silkrouter/tree/feature/ver2
    //

    $.route(function (data) {

        currentAbsoluteRoute = data.route;

        let relativeRoute = currentAbsoluteRoute.split(appBaseRoute).join('');

        console.log("Route absolue", currentAbsoluteRoute, "Route relative", relativeRoute);

        if (_tutorialJson === undefined) {
            loadChapitresJson( function( json ) {
                currentRelativeRoute = updateContentWithRoute( _tutorialJson = json, relativeRoute);
                console.log("currentRelativeRoute", currentRelativeRoute);
            });
        } else {
            currentRelativeRoute = updateContentWithRoute( _tutorialJson, relativeRoute);
        }
     });


    // init translations then init route
    initTutorielTranslations(baseURL, lang, tutorialFolder, function() {
        $.router.init();
    });



    // Retour forcé à l'accueil : par exemple, si l'utilisateur recharge une étape intermédiaire
    function backHome() {
        goRoute('/');
    }

    function goRoute( route ) {
        $.router.set({
            route: appBaseRoute + route
        });
    }


    //
    // Chargement du JSON des templates
    //

    function loadChapitresJson( successCallback ) {
        getTutorielJSON( tutorialFolder + '/json/chapitres.json', function( json ) {
            if(successCallback) {
                successCallback( json );
            }
        });
    }


    //
    // Affichage de la template de la route
    //

    function updateContentWithRoute( tutoJson, route ) {

        console.log("updateContentWithRoute", tutoJson, route);

        // La valeur de la route passée en paramètre peut être modifié ( ex : chapitre )
        let contentDescription = getStepDescriptionForRoute(tutoJson, route);
        let relativeRoute = contentDescription.step.route;

        console.log("relativeRoute", contentDescription, relativeRoute);

        // Préparation des données à injecter dans la template
        let templateParams = beforeLoadingTemplateJsonForRoute( relativeRoute );

        // Chargement de la template
        displayRoute( tutoJson, appBaseRoute, relativeRoute, $header, $content, $footer, contentDescription, templateParams );

        // Application des régles liés à la template après chargement
        afterInsertingTemplateForRoute(relativeRoute);

        return relativeRoute;
    }



    function playVideo(videoElement) {

        if (videoElement) {

            let videoParent = videoElement.closest('.video-parent');

            let video = videoElement[0];
            video.addEventListener('ended', endOfVideo, false);

            var textTracks = video.textTracks;
            if (textTracks && (textTracks.length > 0)) {
                var textTrack = textTracks[0];
                textTrack.mode = "showing";
            }

            if (video.paused === true) {
                video.play();
                videoParent.addClass('is-playing');
            } else {
                video.pause();
                videoParent.removeClass('is-playing');
            }
        }
    }

    function endOfVideo(e) {

        let video = e.target;
        if (video) {
            video.removeEventListener('ended', endOfVideo, false);
        }
        /*
        if ($('.navigation-route.next-link a').length) {
            $('.navigation-route.next-link a').trigger('click');
        } else if ($('.last-video-before-credits')) {
            $('.credits-link a').trigger('click');
        }
        */
    }

})( jQuery );
