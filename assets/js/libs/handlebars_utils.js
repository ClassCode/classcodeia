Handlebars.getTemplate = function(path, name) {
    if (Handlebars.templates === undefined || Handlebars.templates[name] === undefined)    {
        $.ajax({
            url : path + name + '.handlebars',
            success : function(data) {
                if (Handlebars.templates === undefined) {
                    Handlebars.templates = {};
                }
                Handlebars.templates[name] = Handlebars.compile(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("La template Handlebars ", name, " n'a pu être chargée. Vérifiez le chemin : ", path + name + '.handlebars');
                Handlebars.templates[name] = false;
            },
            async : false
        });
    }
    return Handlebars.templates[name];
};
